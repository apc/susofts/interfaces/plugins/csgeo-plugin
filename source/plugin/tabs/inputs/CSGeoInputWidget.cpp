#include "CSGeoInputWidget.hpp"

#include <QContextMenuEvent>
#include <QLocale>
#include <QMimeData>
#include <QVBoxLayout>

#include <CSGeoCore/headers/ProjectCore/DataSet.h>
#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <TDataParameters.h>
#include <editors/text/TextEditor.hpp>
#include <editors/text/TextUtils.hpp>
#include <utils/SettingsManager.hpp>

#include "CSGeoPlugin.hpp"
#include "tabs/common/FileSettingsPanel.hpp"
#include "tabs/common/Utils.hpp"
#include "trinity/CSGeoConfig.hpp"

const QString _ANNOTATION_DOCLINK = QObject::tr(
	R"""(<p>For more information, please refer to the <a href="https://confluence.cern.ch/pages/viewpage.action?spaceKey=SUS&title=CSGeo+User+Guide+-EN">documentation</a>.<p>)""");
const QString _HEADER = QObject::tr(
	R"""(<p>For each coordinate system there is a specific number of values expected for each point. Values must be space or tab separated. In the current coordinate system you should use:</p>)""");
const QString _HEADER_3DCARTESIAN = QObject::tr(R"""(#Name X[m] Y[m] Z[m])""");
const QString _HEADER_2DCARTESIAN = QObject::tr(R"""(#Name X[m] Y[m] H[m])""");
const QString _HEADER_SWISS = QObject::tr(R"""(#Name East(Yswiss)[m] North(Xswiss)[m] H[m])""");
const QString _HEADER_FRENCH = QObject::tr(R"""(#Name East[m] North[m] H[m])""");
const QString _HEADER_GEODETIC_GONS = QObject::tr(R"""(#Name Latitude[Gons] Longitude[Gons] H[m])""");
const QString _HEADER_GEODETIC_DMS = QObject::tr(R"""(#Name Latitude[deg min sec] Longitude[deg min sec] H[m])""");
const QString _HEADER_GEODETIC_DECIDEG = QObject::tr(R"""(#Name Latitude[deg] Longitude[deg] H[m])""");

class CSGeoInputWidget::_CSGeoInputWidget_pimpl
{
public:
	TFileSettingsPanel *settingsPanel = nullptr;
	std::shared_ptr<DataSet> dataset = nullptr;
};

CSGeoInputWidget::CSGeoInputWidget(SPluginInterface *owner, std::shared_ptr<DataSet> dataset, QWidget *parent) :
	TextEditorWidget(owner, parent), _pimpl(std::make_unique<_CSGeoInputWidget_pimpl>())
{
	_pimpl->dataset = dataset;

	// Widget layout
	QVBoxLayout *layout = qobject_cast<QVBoxLayout *>(this->layout());

	// Widgets
	_pimpl->settingsPanel = new TFileSettingsPanel(true, this);
	_pimpl->settingsPanel->setDataParameters(_pimpl->dataset->getDataParameters());
	layout->insertWidget(0, _pimpl->settingsPanel);

	layout->insertSpacerItem(0, new QSpacerItem(0, 31)); // spacer compensating for QTabWidget
	// Editor
	editor()->setMimeTypesSupport(
		{CSGeoPlugin::_baseMimetype().toStdString() + ".pointRAW"}, [this](const QMimeData *mimedata) -> QByteArray { return fromMime(mimedata); },
		[this](const QByteArray &text, QMimeData *mimedata) -> QMimeData * { return toMime(text, mimedata); });
	editor()->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

	// Actions
	auto *actAlig = new QAction(QIcon(":/actions/align-contents"), tr("Reformat file"), this);
	actAlig->setShortcut(tr("Ctrl+Shift+F"));
	actAlig->setToolTip(tr("Reformat the file and align all the columns"));
	actAlig->setStatusTip(tr("Reformat the file and align all the columns"));
	addAction(Menus::edit, actAlig);
	auto *actAddHeader = new QAction(QIcon(":/actions/add-header"), tr("Add header"), this);
	actAddHeader->setToolTip(tr("Add/replace header corresponding to the chosen coordinate system"));
	actAddHeader->setStatusTip(tr("Add/replace header corresponding to the chosen coordinate system"));
	addAction(Menus::edit, actAddHeader);
	additionalContextualActions({actAlig, actAddHeader});

	// Connects
	connect(actAlig, &QAction::triggered, [this]() {
		alignContents(editor());
		annotateAll();
	});
	connect(actAddHeader, &QAction::triggered, this, &CSGeoInputWidget::addHeader);
	connect(_pimpl->settingsPanel, &TFileSettingsPanel::SIG_dataParameterChanged, this, &CSGeoInputWidget::coordsysChanged);
	connect(editor(), &QsciScintillaBase::SCN_CHARADDED, [this](int i) {
		if ((char)i == '\n')
			coordsysChanged();
	});
}

CSGeoInputWidget::~CSGeoInputWidget() = default;

ShareablePointsList CSGeoInputWidget::getContent() const
{
	ShareablePointsList spl;

	return spl;
}

std::shared_ptr<TDataParameters> CSGeoInputWidget::getDataParameters() const
{
	return _pimpl->settingsPanel->getDataParameters();
}

void CSGeoInputWidget::setDataSet(std::shared_ptr<DataSet> dataset)
{
	_pimpl->dataset = dataset;
	_pimpl->settingsPanel->setDataParameters(_pimpl->dataset->getDataParameters());
}

void CSGeoInputWidget::setContent(const ShareablePointsList &)
{
}

void CSGeoInputWidget::updateUi(const QString &pluginName)
{
	TextEditorWidget::updateUi(pluginName);
	if (pluginName != CSGeoPlugin::_name())
		return;
	auto config = SettingsManager::settings().settings<CSGeoConfigObject>(pluginName);
}

bool CSGeoInputWidget::_open(const QString &path)
{
	QFile file(path);
	if (!file.open(QIODevice::OpenModeFlag::ReadOnly | QIODevice::OpenModeFlag::Text))
		return false;
	if (!editor()->read(&file))
		return false;

	editor()->setText(getPointsFromText(editor()->text()));
	alignContents(editor());
	annotateAll();

	updateContent();
	updateUi(owner()->name());
	// Get focus and refresh annotations
	emit editor()->cursorPositionChanged(0, 0);

	return true;
}

void CSGeoInputWidget::_newEmpty()
{
	editor()->setText("Point1 1 2 3\nPoint2 2 3 4\nPoint3 3 4 5\n");
	alignContents(editor());
	editor()->setModified(false);
	annotateAll();
}

QByteArray CSGeoInputWidget::fromMime(const QMimeData *)
{
	return "";
}

template<class T>
inline bool readValue(std::istringstream &sstr, T &value)
{
	sstr >> value;
	return !sstr.fail();
}

QMimeData *CSGeoInputWidget::toMime(const QByteArray &text, QMimeData *mimedata)
{
	// Since we do not have ShareablePointsLists serializer we only insert csv and html type and do not dispatch further
	mimedata->setData("text/html", getHTMLText(text));
	mimedata->setData("text/csv", getSeparatedText(text, !QLocale::system().name().startsWith("en_") ? ';' : ','));
	return mimedata;
}

QString CSGeoInputWidget::getHeader() const
{
	TCoordSysFactory::ECoordSys coordsys = _pimpl->settingsPanel->getDataParameters()->getCoordinateSystem();
	TRefSystemFactory::ERefFrame refFrame = _pimpl->settingsPanel->getDataParameters()->getRefFrameEnumerator();
	if (coordsys == TCoordSysFactory::kGeodetic || coordsys == TCoordSysFactory::kGeodeticSphere)
	{
		TAngle::EUnits unit = _pimpl->settingsPanel->getDataParameters()->getAngleUnits();
		if (unit == TAngle::kDMS)
			return _HEADER_GEODETIC_DMS;
		else if (unit == TAngle::kGons)
			return _HEADER_GEODETIC_GONS;
		else
			return _HEADER_GEODETIC_DECIDEG;
	}
	else if (coordsys == TCoordSysFactory::k2DPlusH)
	{
		if (refFrame == TRefSystemFactory::kLambert93_eh || refFrame == TRefSystemFactory::kFrenchRGF93_CC46_eh || refFrame == TRefSystemFactory::kLambert93_ign69
			|| refFrame == TRefSystemFactory::kFrenchRGF93_CC46_ign69)
			return _HEADER_FRENCH;
#ifdef USE_SWISSTOPO
		else if (refFrame == TRefSystemFactory::kSwissLV03_eh || refFrame == TRefSystemFactory::kSwissLV95_eh || refFrame == TRefSystemFactory::kSwissLV03_ln02
			|| refFrame == TRefSystemFactory::kSwissLV95_ln02 || refFrame == TRefSystemFactory::kSwissLV95_lhn95 || refFrame == TRefSystemFactory::kSwissLV03_lhn95)
			return _HEADER_SWISS;
#endif			
		else
			return _HEADER_2DCARTESIAN;
	}
	else
		return _HEADER_3DCARTESIAN;
}

void CSGeoInputWidget::addHeader()
{
	QString firstLine = editor()->text(0).simplified();
	const QString header = getHeader();
	if (header == firstLine)
		return;
	if (firstLine == _HEADER_3DCARTESIAN || firstLine == _HEADER_2DCARTESIAN 
		|| firstLine == _HEADER_GEODETIC_GONS || firstLine == _HEADER_GEODETIC_DMS	|| firstLine == _HEADER_GEODETIC_DECIDEG
		|| firstLine == _HEADER_FRENCH || firstLine == _HEADER_SWISS)
		editor()->SendScintilla(QsciScintilla::SCI_DELETERANGE, 0, editor()->lineLength(0));

	editor()->insertAt(header + '\n', 0, 0);
	alignContents(editor());
	annotateAll();
}

void CSGeoInputWidget::annotateAll()
{
	const QString completeAnnotation = _HEADER + R"""(<p><strong>)""" + getHeader() + R"""(<\strong></p>)""" + _ANNOTATION_DOCLINK;

	int lineCount = editor()->lines();
	while (lineCount--)
		editor()->annotate(lineCount, completeAnnotation, 0);
}

void CSGeoInputWidget::coordsysChanged()
{
	isModified(true);
	annotateAll();

	// Force update of annotation by imitating movement of a cursor (the same position)
	int line, index;
	editor()->getCursorPosition(&line, &index);
	emit editor()->cursorPositionChanged(line, index);
}
