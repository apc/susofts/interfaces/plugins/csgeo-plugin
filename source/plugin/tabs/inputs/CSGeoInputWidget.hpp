/*
© Copyright CERN 2022. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CSGEOINPUTWIDGET_HPP
#define CSGEOINPUTWIDGET_HPP

#include <memory>

#include <editors/text/TextEditorWidget.hpp>
#include <interface/SGraphicalInterface.hpp>

class ShareablePointsList;
class TDataParameters;
class DataSet;

/**
 * Input editor class for @CSGeoPlugin
 *
 * It provides @TFileSettingsPanel for coordinate system edition and @TextEditorWidget (that it inherits from)
 * for input point edition.
 *
 * When opening a file it parses the input and displays only point definition, without additional header info.
 * It contains @DataSet pointer that should be the same as @Dataset of @Project managed by @CSGeoGraphicalWidget
 * to mantain proper synchronization. This pointer should be provided on construction but in case of opening
 * files it has to be set again (and the old pointer will be invalidated due to zeroing ref count).
 */
class CSGeoInputWidget : public TextEditorWidget
{
	Q_OBJECT

public:
	CSGeoInputWidget(SPluginInterface *owner, std::shared_ptr<DataSet> dataset, QWidget *parent = nullptr);
	virtual ~CSGeoInputWidget() override;

	// SGraphicalWidget
	virtual ShareablePointsList getContent() const override;

	std::shared_ptr<TDataParameters> getDataParameters() const;
	void setDataSet(std::shared_ptr<DataSet> dataset);

public slots:
	// SGraphicalWidget
	virtual void setContent(const ShareablePointsList &spl) override;
	virtual void updateUi(const QString &pluginName) override;

protected:
	// SGraphicalWidget
	virtual bool _save(const QString &) override { return true; } // handled by Project class
	virtual bool _open(const QString &path) override;
	virtual void _newEmpty() override;

private:
	QByteArray fromMime(const QMimeData *mimedata);
	QMimeData *toMime(const QByteArray &text, QMimeData *mimedata);
	/** @return QString annotation corresponding to the used coordinate system */
	QString getHeader() const;

private slots:
	/** Update annotations and modifier flag on change*/
	void coordsysChanged();
	/** Add (or replace) header corresponding to the chosen coordinate system to the first line in editor */
	void addHeader();
	/** Annotate all the lines */
	void annotateAll();

private:
	/** pimpl */
	class _CSGeoInputWidget_pimpl;
	std::unique_ptr<_CSGeoInputWidget_pimpl> _pimpl;
};

#endif // CSGEOINPUTWIDGET_HPP
