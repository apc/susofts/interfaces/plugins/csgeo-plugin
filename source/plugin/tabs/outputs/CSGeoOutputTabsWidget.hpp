/*
© Copyright CERN 2022. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CSGEOOUTPUTTABSWIDGET_HPP
#define CSGEOOUTPUTTABSWIDGET_HPP

#include <memory>

#include <editors/tab/TabEditorWidget.hpp>

class DataSet;

/**
 * Output container class inheriting from @TabEditorWidget to handle outputs for @CSGeoPlugin
 *
 * It stores multiple @CSGeoOutputWidget with pointers to @Dataset of main @Project. It enables communication
 * between @Project and @CSGeoOutputWidget and besides creating new tabs and removing them it does not provide
 * any editing features. The tabs @DataSet should be synchronized with @Project::outputData in @CSGeoGraphicalWidget.
 */
class CSGeoOutputTabsWidget : public TabEditorWidget
{
	Q_OBJECT

public:
	CSGeoOutputTabsWidget(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~CSGeoOutputTabsWidget() override;

public slots:
	/** Add new tab to the project*/
	void addTab(std::shared_ptr<DataSet> dataset);
	/** Open outputs for all tabs */
	bool openOutputs();
	/** Middle mouse button filter for close tab */
	virtual bool eventFilter(QObject *watched, QEvent *event);

signals:
	/** Close requested by clicking close button or middle mouse button */
	void tabCloseRequested(int index);
	/** Add new tab requested by clicking the button on right-top side of the widget */
	void tabAddRequested();

protected:
	// STabInterface
	virtual bool _save(const QString &) override { return true; } // handled by Project class
	virtual bool _open(const QString &) override { return true; } // handled by Project class
	virtual void _newEmpty() override {}

private:
	void tabClose(int index);

private:
	/** pimpl */
	class _CSGeoOutputTabsWidget_pimpl;
	std::unique_ptr<_CSGeoOutputTabsWidget_pimpl> _pimpl;
};

#endif // CSGEOOUTPUTTABSWIDGET_HPP
