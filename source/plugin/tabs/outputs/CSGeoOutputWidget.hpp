/*
© Copyright CERN 2022. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CSGEOOUTPUTWIDGET_HPP
#define CSGEOOUTPUTWIDGET_HPP

#include <memory>

#include <interface/SGraphicalInterface.hpp>

class DataSet;
class QMimeData;
class QByteArray;

/**
 * Output editor class for @CSGeoPlugin
 *
 * It provides @TFileSettingsPanel for coordinate system edition and @TextEditor to view the output file.
 * Methods like @_save and @_newEmpty are not handled by this class but by @Project via
 * @CSGeoGraphicalWidget.
 */
class CSGeoOutputWidget : public SGraphicalWidget
{
	Q_OBJECT

public:
	CSGeoOutputWidget(SPluginInterface *owner, std::shared_ptr<DataSet> dataset, QWidget *parent = nullptr);
	virtual ~CSGeoOutputWidget() override;

	// Inherited via SGraphicalWidget
	virtual ShareablePointsList getContent() const override;
	virtual QString getPath() const override;

public slots:
	virtual void updateContent() override;

private:
	// Inherited via SGraphicalWidget
	virtual bool _save(const QString &) override { return true; } // save in Project class
	virtual bool _open(const QString &) override;
	virtual void _newEmpty() override {}

	QByteArray fromMime(const QMimeData *mimedata);
	QMimeData *toMime(const QByteArray &text, QMimeData *mimedata);

private:
	/** pimpl */
	class _CSGeoOutputWidget_pimpl;
	std::unique_ptr<_CSGeoOutputWidget_pimpl> _pimpl;
};

#endif // CSGEOOUTPUTWIDGET_HPP
