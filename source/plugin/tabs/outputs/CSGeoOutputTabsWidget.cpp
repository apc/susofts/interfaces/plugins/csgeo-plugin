#include "CSGeoOutputTabsWidget.hpp"

#include <QEvent>
#include <QKeyEvent>
#include <QPushButton>
#include <QSizePolicy>
#include <QTabBar>
#include <QTabWidget>
#include <QVBoxLayout>

#include <editors/text/TextUtils.hpp>
#include <interface/SPluginInterface.hpp>

#include "tabs/outputs/CSGeoOutputWidget.hpp"

class CSGeoOutputTabsWidget::_CSGeoOutputTabsWidget_pimpl
{
public:
	std::vector<CSGeoOutputWidget *> outputs;
};

CSGeoOutputTabsWidget::CSGeoOutputTabsWidget(SPluginInterface *owner, QWidget *parent) :
	TabEditorWidget(owner, parent), _pimpl(std::make_unique<_CSGeoOutputTabsWidget_pimpl>())
{
	sameDescriptionMode(false);

	// tab
	tab()->setTabBarAutoHide(false);
	tab()->setTabsClosable(true);
	tab()->tabBar()->setExpanding(true);
	tab()->tabBar()->installEventFilter(this);

	// add new button
	QPushButton *addButton = new QPushButton(QIcon(":/actions/add"), "", tab());
	addButton->setStyleSheet("QPushButton{background-color:rgba(180, 255, 200, 180);}");
	addButton->setIconSize(QSize(18, 18));
	addButton->setFixedSize(QSize(27, 27));
	tab()->setCornerWidget(addButton);

	// connects
	connect(addButton, &QPushButton::clicked, this, &CSGeoOutputTabsWidget::tabAddRequested);
	connect(tab(), &QTabWidget::tabCloseRequested, this, &CSGeoOutputTabsWidget::tabClose);
}

CSGeoOutputTabsWidget::~CSGeoOutputTabsWidget() = default;

void CSGeoOutputTabsWidget::addTab(std::shared_ptr<DataSet> dataset)
{
	CSGeoOutputWidget *widget = new CSGeoOutputWidget(owner(), dataset, this);
	tab()->addTab(widget, widget->windowTitle());
	_pimpl->outputs.push_back(widget);
	widget->isModified(true);
	tab()->setCurrentWidget(widget);
	emit hasChanged();
}

bool CSGeoOutputTabsWidget::openOutputs()
{
	for (auto widget : _pimpl->outputs)
	{
		if (!widget->open(widget->getPath()))
			return false;
	}
	return true;
}

bool CSGeoOutputTabsWidget::eventFilter(QObject *watched, QEvent *event)
{
	QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(event);
	if (!mouseEvent || mouseEvent->type() == QEvent::MouseButtonPress || mouseEvent->button() != Qt::MiddleButton || watched != tab()->tabBar())
		return false;

	int index = tab()->tabBar()->tabAt(mouseEvent->pos());
	if (index == -1)
		return false;

	tabClose(index);
	return true;
}

void CSGeoOutputTabsWidget::tabClose(int index)
{
	if (tab()->count() == 1) // Do not close the last tab
		return;
	tab()->removeTab(index);
	_pimpl->outputs[index]->deleteLater();
	_pimpl->outputs.erase(_pimpl->outputs.begin() + index);
	emit tabCloseRequested(index);
	isModified(true);
	emit hasChanged();
}
