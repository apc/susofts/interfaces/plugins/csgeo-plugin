#include "CSGeoOutputWidget.hpp"

#include <fstream>
#include <sstream>

#include <QFile>
#include <QFileInfo>
#include <QLocale>
#include <QMimeData>
#include <QVBoxLayout>

#include <CSGeoCore/headers/ProjectCore/DataSet.h>
#include <ProjectFramework/headers/ProjectData/TDataParameters.h>
#include <QtStreams.hpp>
#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <editors/text/TextEditor.hpp>
#include <editors/text/TextUtils.hpp>
#include <utils/ClipboardManager.hpp>
#include <utils/SettingsManager.hpp>
#include <utils/StylesHelper.hpp>

#include "CSGeoOutputTabsWidget.hpp"
#include "CSGeoPlugin.hpp"
#include "tabs/common/FileSettingsPanel.hpp"
#include "tabs/common/Utils.hpp"
#include "trinity/CSGeoConfig.hpp"

class CSGeoOutputWidget::_CSGeoOutputWidget_pimpl
{
public:
	TextEditor *editor = nullptr;
	TFileSettingsPanel *settingsPanel = nullptr;
	QFileInfo path;
	std::shared_ptr<DataSet> dataset = nullptr;
};

CSGeoOutputWidget::CSGeoOutputWidget(SPluginInterface *owner, std::shared_ptr<DataSet> dataset, QWidget *parent) :
	SGraphicalWidget(owner, parent), _pimpl(std::make_unique<_CSGeoOutputWidget_pimpl>())
{
	_pimpl->dataset = dataset;
	updateUi(owner->name());

	// Widget layout
	QVBoxLayout *layout = new QVBoxLayout(this);
	QWidget *widget = new QWidget(this);
	widget->setLayout(layout);
	setWidget(widget);

	// Widgets
	_pimpl->editor = new TextEditor(this);
	_pimpl->editor->setReadOnly(true);
	setBackground(_pimpl->editor, getThemeColor(QColor(240, 240, 240)));
	_pimpl->settingsPanel = new TFileSettingsPanel(true, this, false);
	_pimpl->settingsPanel->setDataParameters(_pimpl->dataset->getDataParameters());
	layout->addWidget(_pimpl->settingsPanel);
	layout->addWidget(_pimpl->editor);


	// Setup editor
	_pimpl->editor->setMimeTypesSupport(
		{CSGeoPlugin::_baseMimetype().toStdString() + ".pointRAW"}, [this](const QMimeData *mimedata) -> QByteArray { return fromMime(mimedata); },
		[this](const QByteArray &text, QMimeData *mimedata) -> QMimeData * { return toMime(text, mimedata); });
	_pimpl->editor->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

	// Disable docks
	setStatusWidget(nullptr);
	setHelpWidget(nullptr);
	setToolBar(nullptr);
	setWindowTitle("New Output");

	// Connects
	connect(_pimpl->settingsPanel, &TFileSettingsPanel::SIG_dataParameterChanged, this, &CSGeoOutputWidget::updateContent);
	connect(_pimpl->settingsPanel, &TFileSettingsPanel::SIG_dataParameterChanged, [this]() { isModified(true); });
}

CSGeoOutputWidget::~CSGeoOutputWidget() = default;

ShareablePointsList CSGeoOutputWidget::getContent() const
{
	return ShareablePointsList();
}

QString CSGeoOutputWidget::getPath() const
{
	return QString::fromStdWString(_pimpl->dataset->getAbsolutePath());
}

void CSGeoOutputWidget::updateContent()
{
	setWindowTitle(QString::fromStdString(_pimpl->dataset->getDataParameters()->getRFName()));
}

bool CSGeoOutputWidget::_open(const QString &path)
{
	_pimpl->path = path;
	QFile file(path);
	if (!file.open(QIODevice::OpenModeFlag::ReadOnly | QIODevice::OpenModeFlag::Text))
		return false;
	if (!_pimpl->editor->read(&file))
		return false;

	_pimpl->editor->setReadOnly(false);
	_pimpl->editor->setText(getPointsFromText(_pimpl->editor->text()));
	alignContents(_pimpl->editor);
	_pimpl->editor->setReadOnly(true);

	updateContent();
	setWindowTitle(QString::fromStdString(_pimpl->dataset->getReferenceFrame()->getName()));

	return true;
}

QByteArray CSGeoOutputWidget::fromMime(const QMimeData *)
{
	return "";
}

QMimeData *CSGeoOutputWidget::toMime(const QByteArray &text, QMimeData *mimedata)
{
	// Since we do not have ShareablePointsLists serializer we only insert csv and html type and do not dispatch further
	mimedata->setData("text/html", getHTMLText(text));
	mimedata->setData("text/csv", getSeparatedText(text, !QLocale::system().name().startsWith("en_") ? ';' : ','));
	return mimedata;
}
