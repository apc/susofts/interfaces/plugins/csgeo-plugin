/*
© Copyright CERN 2022. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CSGEOUTILS_HPP
#define CSGEOUTILS_HPP

// Since currently CSGeoInputWidget and CSGeoOutputWidget do not inherit from the same base class
// this file is used to share some functionalities between them without code duplication

class QString;

/** Having the text from input/output files, skip all the comment/header contents and output just the list of points. */
QString getPointsFromText(const QString &text);

#endif // CSGEOUTILS_HPP
