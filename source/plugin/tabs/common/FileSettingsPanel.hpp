/*!
	FileSettingsPanel.h

	Class displaying the Reference Frame panel. After the user will have selected the RF, the point model's adapter is updated.
	The widget is composed by :
		- fRefFrameList: a reduced list of the reference frame
		- some combobox to expend this last list (french system, swiss system, CERN XYH system, geoid model or ellipsoid model)
		- widgets allowing to define an origin if required
		- the precision (number of digits for the coordinates)
		- the coordinate system
		- the solution and epoch for the ITRF and ETRF reference frames
			
	All the parameters are stored in a shared pointer of TDataParameter to be able to update the core (DataSet) with the view and vice versa through the Supervisor.
	Some signals and slots are directly implemented in this class. They handle the ComboBox. Only the required ComboBox are enabled according to the current reference frame.
*/

#ifndef TFILE_SETTINGS_PANEL
#define TFILE_SETTINGS_PANEL

#include <TRefSystemFactory.h>
#include <TLocalSystemOrigin.h>

#include <QDialog>
#include <memory>

class TDataParameters;
class OriginLoaderPanel;
class OriginEditor;
class QStandardItemModel;
class QComboBox;
class QLabel;
class QButtonGroup;
class QListView;
class QDialog;
class QLineEdit;
class QDoubleSpinBox;

class TFileSettingsPanel : public QWidget
{
	Q_OBJECT

public:

	/// Constructor
	TFileSettingsPanel(bool showPrecisionSelector, QWidget* parent = 0, bool isInput = true);

	/// Returns the TdataParameter corresponding to the selected settings
	std::shared_ptr<TDataParameters> getDataParameters() { return fDataParams; };
	void setDataParameters(std::shared_ptr<TDataParameters> params);

	/// update the view from dataset
	void updateViewFromDataSetParameters(std::shared_ptr<TDataParameters> params);

	/// set enable all the widget
	void setEnable(bool enable);

	//To reinitialize the view. Empty the LineEdit.
	void cleanPanel(TDataParameters fData);

	//check if the reference frame is completly set
	bool isReferenceFrameConsistent();

signals:

	void SIG_dataParameterChanged(std::shared_ptr<TDataParameters>, bool);


private slots:

    /// Manage the fAngleUnit and fAngleUnitLabel visibility depending on the coordinatinate system
	void sCoordSysClicked(int index);

	/// Manage the precision
	void sPrecisionChanged(int index);

	/// Manage reference system from the QListView, 2 signals are emited from the QListView
	//slot called when the current index changed
	void SModelChanged(const QModelIndex &index);
	// slot called to manage the click on the same index
	void SClick(const QModelIndex &index);
	/// Manage LA and MLA system with the geoid model (fGeoid ComboBox)
	void sManageGeoid(const int &index);
	/// Manage LG and MLG system with the ellipsoid model (fEllipsoid ComboBox)
	void sManageEllipsoid(const int &index);
	/// Manage CGRF system with the surface reference (fCGRF ComboBox)
	void sManageCGRF(const int &index);
	/// Manage the french reference system from the fSwiss ComboBox
	void sManageFrenchSytem(const int &index);
	/// Manage the Swiss reference system from the fFrench ComboBox
	void sManageSwissSystem(const int &index);
	/// Manage the CERN reference system from the fXYHg ComboBox
	void sManageCernSystem(const int &index);

	/// Manage origin changes
	void sOriginChanged();
	/// Manage the type of origin declaration (from a file or manually)
	void sOriginClicked(int index);

	/// Manage ITRF solution changes
	void sITRFChanged(const int& index);
	/// Manage ETRF solution changes
	void sETRFChanged(const int& index);

	/// Manage epoch changes
	void sEpochChanged(const double& coordEpoch);




private:

	/// Create the UI
	void setupUi();
	/// Initialize the reference frame list
	void setupRefFrameList();
	/// Initialze all the combobox
	void setupComboBox();

	/// Manage reference system from the QListView
	void manageRefFrame(const QModelIndex &index);
	/// Set the coordinate system available
	void refFrameChanged(TRefSystemFactory::ERefFrame frame);

	/// Manage units depending the coordinates system
	void setCoordSys(TCoordSysFactory::ECoordSys system);

	/// Sets the coordinate system available in the combobox
	void setCoordinateSystem(TRefSystemFactory::ERefFrame frame);
	/// Return the coordinate system
	TCoordSysFactory::ECoordSys getCoordSys() const;
	/// Empty the coordinate system combobox
	void resetCoordinateSystem();

	/// Return angle unit
	TAngle::EUnits getUnits() const;

	// Set visible the orign data when needed
	void setOriginVisible(bool state);

	//hide the list after "Other" if the current reference system is one of the five first ones.
	void hideRFList(TRefSystemFactory::ERefFrame);

	/// Bits specifying which parts of the origin dialog are set active in the interface
	static const int kEnableOriginCoords = (1 << 0);
	static const int kEnableOriginRotation = (1 << 1);

	/// Reduced list of reference frames
	QListView * fRefFrameList;
	QStandardItemModel* fRefFrameModel;
	QLabel * fComment;

	/// Origin
	OriginLoaderPanel* fOriginLoaderPanel;
	OriginEditor* fOriginEditor;
	QButtonGroup *originGroup;
	bool fOriginFileLoaderVisible = false;

	// Combobox and Qlabel to reduce the list and be able to defined all reference system.
	QComboBox * fCGRF;
	QComboBox * fGeoid;
	QComboBox * fEllipsoid;
	QComboBox * fSwiss;
	QComboBox * fFrench;
	QComboBox* fITRFSolution;
	QComboBox* fETRFSolution;
	QComboBox * fXYHg;
	QLabel * fCGRFLabel;
	QLabel * fGeoidLabel;
	QLabel * fEllipsoidLabel;
	QLabel * fSwissLabel;
	QLabel * fFrenchLabel;
	QLabel * fITRFSolutionLabel;
	QLabel * fETRFSolutionLabel;
	QLabel* fEpochLabel;
	QLabel * fXYHgLabel;

	//Other parameters
	QComboBox * fAngleUnit;
	QLabel * fAngleUnitLabel;
	QComboBox * fPrecision;
	QLabel * fPrecisionLabel;
	QComboBox * fCooSystem;
	QLabel * fCooSystemLabel;
	QDoubleSpinBox * fEpochEdit;

	int rowIndexList; /// Row index of the reference frame list
	TRefSystemFactory::ERefFrame fFrameSystem; /// Store the reference frame for getDataParameter()
	std::shared_ptr<TDataParameters> fDataParams; /// Shared pointer to a TDataParameter
	bool fShowPrecisionSelector = true; /// Precision
	bool input = true; /// to know in which FileSettingsPanel we are

	// boolean to manage signal conflict with the fRefFrameList QListView
	bool alreadyCalled = false;
};

#endif
