/*
© Copyright CERN 2022. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#include "tabs/common/Utils.hpp"

#include <editors/text/TextUtils.hpp>

QString getPointsFromText(const QString &text)
{
	QStringList contents_split = text.split('\n');
	QMutableStringListIterator iter(contents_split);
	while (iter.hasNext())
	{
		QString &val = iter.next();
		val = val.trimmed();
		if (val.isEmpty()
			|| val.startsWith('#')
			// in case that point names begin with the keywords below
			|| (val.indexOf("ORIGIN") != -1 && val.split(' ', QString::SplitBehavior::SkipEmptyParts).size() >= 7)
			|| (val.indexOf("ITRF") != -1 && val.split(' ', QString::SplitBehavior::SkipEmptyParts).size() == 5)
			|| (val.indexOf("ETRF") != -1 && val.split(' ', QString::SplitBehavior::SkipEmptyParts).size() == 5))
			iter.remove();
	}

	return contents_split.join('\n');
}
