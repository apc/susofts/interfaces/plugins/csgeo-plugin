/// SurveyLib
#include <TLocalSystemOrigin.h>
#include <TRefFrameInfo.h>
#include <TSpatialPosition.h>

/// CSGeoCore
#include <CSGeoCore/headers/io/input_point/TOriginReader.h>

// Plugin
#include "tabs/common/OriginLoaderPanel.hpp"

/// Qt
#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>

/// StdLib
#include <fstream>
#include <string>

OriginLoaderPanel::OriginLoaderPanel(QWidget *parent) :
	QWidget(parent),
	isOriginSet(false),
	fName("origin"),
	fOrigin(TSpatialPosition(TRefFrameInfo::getReferenceFrame(TRefSystemFactory::kCCS), 0.0, 0.0, 0.0, TCoordSysFactory::k3DCartesian)),
	fGisement(0.0),
	fSlope(0.0)
{
	ui.setupUi(this);
	ui.browseButton->setIconSize(QSize(23, 20));
	ui.browseButton->setFlat(true);
	ui.browseButton->setGeometry(QRect(72, 15, 23, 20));
	connect(ui.browseButton, SIGNAL(clicked()), this, SLOT(sBrowseButton()));
}

void OriginLoaderPanel::readOrigin()
{
	std::ifstream input(ui.filePath->text().toStdString());
	try
	{
		const TLocalSystemOrigin &ori = TOriginReader().readOriginFromStream(input);
		fName = ori.name();
		fOrigin = ori.origin();
		fGisement = ori.gisement();
		fSlope = ori.slope();
	}
	catch (const std::logic_error &)
	{
		fName = "origin";
		fOrigin = TSpatialPosition(TRefFrameInfo::getReferenceFrame(TRefSystemFactory::kCCS), 0.0, 0.0, 0.0, TCoordSysFactory::k3DCartesian);
		fGisement = TAngle(0.0);
		fSlope = TAngle(0.0);
		ui.filePath->clear();
		throw std::logic_error("Problem reading the origin file.");
	}
}

bool OriginLoaderPanel::loadOrigin(std::shared_ptr<TDataParameters> fData)
{
	if (fData && fData->getLocalSystemOrigin())
	{
		fName = fData->getLocalSystemOrigin()->name();
		fOrigin = fData->getLocalSystemOrigin()->origin();
		fGisement = fData->getLocalSystemOrigin()->gisement();
		fSlope = fData->getLocalSystemOrigin()->slope();

		ui.filePath->setText(fData->getOriginFile().c_str());
		return true;
	}
	fName = "origin";
	fOrigin = TSpatialPosition(TRefFrameInfo::getReferenceFrame(TRefSystemFactory::kCCS), 0.0, 0.0, 0.0, TCoordSysFactory::k3DCartesian);
	fGisement = TAngle(0.0);
	fSlope = TAngle(0.0);

	ui.filePath->clear();
	return false;
}

void OriginLoaderPanel::sBrowseButton()
{
	QFileInfo f = QFileDialog::getOpenFileName(this, tr("Origin file"), "", tr("Origin files (*.ori);;All files (*.*)"));

	// If the FileDialog is canceled
	if (!f.exists())
	{
		isOriginSet = false;
		emit SIG_OriginChanged();
		return;
	}

	ui.filePath->setText(f.filePath());

	// read the origin
	readOrigin();
	isOriginSet = true;

	// emit signal to set the lso in the settingsPanel's dataparameter
	emit SIG_OriginChanged();
}
