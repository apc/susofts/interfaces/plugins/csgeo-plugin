#include "tabs/common/FileSettingsPanel.hpp"

#include "tabs/common/OriginEditor.hpp"
#include "tabs/common/OriginLoaderPanel.hpp"

#include <TRefFrameInfo.h>
#include <TGeodeticRefFrame.h>
#include <TTerrestrialReferenceFrame.h>
#include <TTrf2TrfTransformation.h>

#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QMessageBox>
#include <QDebug>
#include <QStandardItemModel>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QListView>
#include <QtWidgets/QButtonGroup>
#include <QModelIndexList>

enum RadioButtonEnum
{
	kNotSelected = -1,
	kFromFile = 1,
	kFromEditor = 2
};


TFileSettingsPanel::TFileSettingsPanel(bool showPrecisionSelector, QWidget * parent, bool isInput)
: QWidget(parent)
, input(isInput)
, fComment(new QLabel(this))
, fDataParams(std::make_shared<TDataParameters>())
, fRefFrameList(new QListView(this))
, fRefFrameModel(new QStandardItemModel(this))
, fCooSystem(new QComboBox(this))
, fCooSystemLabel(new QLabel(this))
, fAngleUnit(new QComboBox(this))
, fAngleUnitLabel(new QLabel(this))
, originGroup(new QButtonGroup(this))
, fPrecision(new QComboBox(this))
, fPrecisionLabel(new QLabel(this))
, fShowPrecisionSelector(showPrecisionSelector)
, fCGRF(new QComboBox(this))
, fGeoid(new QComboBox(this))
, fEllipsoid(new QComboBox(this))
, fSwiss(new QComboBox(this))
, fFrench(new QComboBox(this))
, fITRFSolution(new QComboBox(this))
, fETRFSolution(new QComboBox(this))
, fEpochEdit(new QDoubleSpinBox(this))
, fXYHg(new QComboBox(this))
, fCGRFLabel(new QLabel(this))
, fGeoidLabel(new QLabel(this))
, fEllipsoidLabel(new QLabel(this))
, fSwissLabel(new QLabel(this))
, fFrenchLabel(new QLabel(this))
, fITRFSolutionLabel(new QLabel(this))
, fETRFSolutionLabel(new QLabel(this))
, fXYHgLabel(new QLabel(this))
, fEpochLabel(new QLabel(this))
, rowIndexList(0)
, fFrameSystem(TRefSystemFactory::ERefFrame::kNotInGraph)
{
	fOriginEditor = new OriginEditor(this);
	fOriginLoaderPanel = new OriginLoaderPanel(this);

	fComment->setWordWrap(true);
	setupComboBox();
	setupUi();
	setupRefFrameList();
	
	//selection changed with the key navigation
	connect(fRefFrameList->selectionModel(), SIGNAL(currentChanged(QModelIndex, QModelIndex)), this, SLOT(SModelChanged(QModelIndex)));
	// the click signal has an effect only  when the 4th index is click.
	connect(fRefFrameList, SIGNAL(clicked(const QModelIndex &)), this, SLOT(SClick(const QModelIndex &)));
	
	connect(fCGRF, SIGNAL(currentIndexChanged(const int &)), this, SLOT(sManageCGRF(const int &)));
	connect(fGeoid, SIGNAL(currentIndexChanged(const int &)), this, SLOT(sManageGeoid(const int &)));
	connect(fEllipsoid, SIGNAL(currentIndexChanged(const int &)), this, SLOT(sManageEllipsoid(const int &)));
	connect(fSwiss, SIGNAL(currentIndexChanged(const int &)), this, SLOT(sManageSwissSystem(const int &)));
	connect(fFrench, SIGNAL(currentIndexChanged(const int &)), this, SLOT(sManageFrenchSytem(const int &)));
	connect(fXYHg, SIGNAL(currentIndexChanged(const int &)), this, SLOT(sManageCernSystem(const int &)));
	
	connect(fCooSystem, SIGNAL(currentIndexChanged(int)), this, SLOT(sCoordSysClicked(int)));
	connect(fAngleUnit, SIGNAL(currentIndexChanged(int)), this, SLOT(sCoordSysClicked(int)));
	connect(fPrecision, SIGNAL(currentIndexChanged(int)), this, SLOT(sPrecisionChanged(int)));
	connect(originGroup, SIGNAL(buttonClicked(int)), this, SLOT(sOriginClicked(int)));

	connect(fEpochEdit, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &TFileSettingsPanel::sEpochChanged);
	connect(fITRFSolution, SIGNAL(currentIndexChanged(int)), this, SLOT(sITRFChanged(const int&)));
	connect(fETRFSolution, SIGNAL(currentIndexChanged(int)), this, SLOT(sETRFChanged(const int&)));
	

	//connect origin UI to this to be able to allow to add point only when origin is completly filled.
	connect(fOriginEditor, SIGNAL(SIG_OriginChanged()), this, SLOT(sOriginChanged()));
	connect(fOriginLoaderPanel, SIGNAL(SIG_OriginChanged()), this, SLOT(sOriginChanged()));

}


void TFileSettingsPanel::refFrameChanged(TRefSystemFactory::ERefFrame frame)
{
	fFrameSystem = frame;
	setCoordinateSystem(frame);
	fDataParams->setRefFrame(fFrameSystem);
	sPrecisionChanged(fPrecision->currentData().toInt());

	hideRFList(fFrameSystem);

	emit SIG_dataParameterChanged(fDataParams, input);
	sOriginChanged();
}

void TFileSettingsPanel::SClick(const QModelIndex &index)
{
	// enter in manageRefFrame only if the slot SModelChanged has not yet been called by the signal currentChanged(QModelIndex, QModelIndex), use the boolean alreadyCalled to know that
	if (rowIndexList == index.row() && rowIndexList ==4 && !alreadyCalled)
		manageRefFrame(index);
	else
		alreadyCalled = false;
}

void TFileSettingsPanel::SModelChanged(const QModelIndex &index)
{
	if (rowIndexList != index.row())
	{
		manageRefFrame(index);
		if (rowIndexList == 4)
			alreadyCalled = true;
	}
	else
		return;
}



void TFileSettingsPanel::manageRefFrame(const QModelIndex &index)
{
	int row = index.row();
	rowIndexList = row;

	auto manageVisibility = [&](bool b1 , bool b2, bool b3, bool b4, bool b5, bool b6, bool b7, bool b8, bool b9, bool b10, bool b11, bool b12, bool b13, bool b14, bool b15, 
		bool b16, bool b17, bool b18, bool b19)
	{
	setOriginVisible(b1);
	fCGRF->setVisible(b2);
	fGeoid->setVisible(b3);
	fEllipsoid->setVisible(b4);
	fSwiss->setVisible(b5);
	fFrench->setVisible(b6);
	fXYHg->setVisible(b7);

	fCGRFLabel->setVisible(b8);
	fGeoidLabel->setVisible(b9);
	fEllipsoidLabel->setVisible(b10);
	fSwissLabel->setVisible(b11);
	fFrenchLabel->setVisible(b12);
	fXYHgLabel->setVisible(b13); 
	fITRFSolution->setVisible(b14);
	fETRFSolution->setVisible(b15);
	fITRFSolutionLabel->setVisible(b16);
	fETRFSolutionLabel->setVisible(b17);
	fEpochEdit->setVisible(b18);
	fEpochLabel->setVisible(b19);
	};

	switch (row)
	{
	case 0: //ccs
		manageVisibility(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false);
		fComment->setText("CERN coordinate system: 3D Cartesian coordinate systembased on the CGRF. Use for the positioning of the accelerator lines.");
		refFrameChanged(TRefSystemFactory::ERefFrame::kCCS);
		break;

	case 1: //cgrf
		manageVisibility(false, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false);
		sManageCGRF(fCGRF->currentIndex());
		break;


	case 2: //mla
		fGeoid->setMaxVisibleItems(6); //P0 is not allowed in a MLA
		manageVisibility(true, false, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false);
		fOriginEditor->setBearing(true);
		sManageGeoid(fGeoid->currentIndex());
		break;

	case 3: //xyhg
		manageVisibility(false, false, false, false, false, false, true, false, false, false, false, false, true, false, false, false, false, false, false);
		sManageCernSystem(fXYHg->currentIndex());
		break;

	case 4: //others
		if (fRefFrameList->isRowHidden(5))
			for (int i = 5; i < 18; i++)
				fRefFrameList->setRowHidden(i, false);
		else
			for (int i = 5; i < 18; i++)
				fRefFrameList->setRowHidden(i, true);
		manageVisibility(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false);
		resetCoordinateSystem(); 
		fComment->setText("more reference frames");
		break;

	case 5: //itrf
		manageVisibility(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false);
		fComment->setText("The ITRF 97 is a geocentric system based on the GRS80 ellipsoid. Its origin meridian is the Greenwich meridian. The reference epoch is 1998.5");
		refFrameChanged(TRefSystemFactory::ERefFrame::kITRF97);
		break;

	case 6: //wgs84 (G2139)
		manageVisibility(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false);
		fComment->setText("Latest realization of the WGS84 datum (G2139). It is aligned with ITRF2014 at epoch 2016.0. Geodetic coordinates are given on the WGS84 ellipsoid");
		refFrameChanged(TRefSystemFactory::ERefFrame::kWGS84_G2139);
		break;

	case 7: //etrf
		manageVisibility(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false);
		fComment->setText("The ETRF is a geocentric system based on the GRS80 ellipsoid. Its origin meridian is the Greenwich meridian. It is a densification of the ITRF and is co-mobile with the Eurasian plate.");
		refFrameChanged(TRefSystemFactory::ERefFrame::kETRF93);
		break;

	case 8: //swiss
		manageVisibility(false, false, false, false, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false);
		sManageSwissSystem(fSwiss->currentIndex());
		break;

	case 9: //french
		manageVisibility(false, false, false, false, false, true, false, false, false, false, false, true, false, false, false, false, false, false, false);
		sManageFrenchSytem(fFrench->currentIndex());
		break;

	case 10: //xhe
		manageVisibility(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false);
		fComment->setText("X, Y coordinates from the CCS, the height is calculated with the ellipsoid.");
		refFrameChanged(TRefSystemFactory::ERefFrame::kCernXYHe);
		break;

	case 11: //x0y0he
		manageVisibility(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false);
		fComment->setText("X, Y coordinates from the CCS, the height is calculated with the ellipsoid at P0.");
		refFrameChanged(TRefSystemFactory::ERefFrame::kCernX0Y0He);
		break;

	case 12: //lg 
		fEllipsoid->setMaxVisibleItems(3); //P0 is allowed in a LG
		manageVisibility(true, false, false, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false);
		fOriginEditor->setBearing(false);
		sManageEllipsoid(fEllipsoid->currentIndex());
		break;

	case 13: //mlg
		fEllipsoid->setMaxVisibleItems(2); //P0 is not allowed in a MLG
		manageVisibility(true, false, false, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false);
		fOriginEditor->setBearing(true);
		sManageEllipsoid(fEllipsoid->currentIndex());
		break;

	case 14: //la
		fGeoid->setMaxVisibleItems(7); //P0 is allowed in a LA
		manageVisibility(true, false, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false);
		fOriginEditor->setBearing(false);
		sManageGeoid(fGeoid->currentIndex());
		break;


	case 15: //itrf (specify solution and epoch)
		manageVisibility(false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, true, false, true, true);
		fComment->setText("The ITRF is a geocentric system based on the GRS80 ellipsoid. Specify the solution and epoch of the coordinates.");
		if (input) {
			refFrameChanged(TRefSystemFactory::ERefFrame::kITRFin);
			sITRFChanged(fITRFSolution->currentIndex());
		}
		else
		{
			refFrameChanged(TRefSystemFactory::ERefFrame::kITRFout);
			sITRFChanged(fITRFSolution->currentIndex());
		}
		
		break;

	case 16: //etrf (specify solution and epoch)
		manageVisibility(false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, true, true, true);
		fComment->setText("The ETRF is a geocentric system based on the GRS80 ellipsoid. It is a densification of the ITRF and is co-mobile with the Eurasian plate. Specify the solution and epoch of the coordinates. ");
		if (input) {
			refFrameChanged(TRefSystemFactory::ERefFrame::kETRFin);
			sETRFChanged(fETRFSolution->currentIndex());
		}
		else
		{
			refFrameChanged(TRefSystemFactory::ERefFrame::kETRFout);
			sETRFChanged(fETRFSolution->currentIndex());
		}
		break;

	}
	
}

void TFileSettingsPanel::sManageGeoid(const int &index)
{

	switch (rowIndexList)
	{
	case 14: //LA
		
		switch (index)
		{
		case 0 :
			setOriginVisible(true);
			fComment->setText("A local astronomical system (LA) at P has his vertical axis in the same direction of the vertical of P and the Y axis oriented in the north direction. The geodetic reference system is the sphere. <a href = \"https://edms.cern.ch/ui/file/107906/0/georefsys.pdf\">  &Sigma;DMS</a>");
			refFrameChanged(TRefSystemFactory::ERefFrame::kLASphere);
			break;
		case 1:
			setOriginVisible(true);
			fComment->setText("A local astronomical system (LA) at P has his vertical axis in the same direction of the vertical of P and the Y axis oriented in the north direction. The geodetic reference system is the geoid CG1985 at the level 0. <a href = \"https://edms.cern.ch/ui/file/107906/0/georefsys.pdf\">  &Sigma;DMS</a>");
			refFrameChanged(TRefSystemFactory::ERefFrame::kLA1985H0);
			break;
		case 2:
			setOriginVisible(true);
			fComment->setText("A local astronomical system (LA) at P has his vertical axis in the same direction of the vertical of P and the Y axis oriented in the north direction. The geodetic reference system is the geoid CG1985 at the machine level. <a href = \"https://edms.cern.ch/ui/file/107906/0/georefsys.pdf\">  &Sigma;DMS</a>");
			refFrameChanged(TRefSystemFactory::ERefFrame::kLA1985Machine);
			break;
		case 3:
			setOriginVisible(true);
			fComment->setText("A local astronomical system (LA) at P has his vertical axis in the same direction of the vertical of P and the Y axis oriented in the north direction. The geodetic reference system is the geoid CG2000 at level 0. <a href = \"https://edms.cern.ch/ui/file/107906/0/georefsys.pdf\">  &Sigma;DMS</a>");
			refFrameChanged(TRefSystemFactory::ERefFrame::kLA2000H0);
			break;
		case 4:
			setOriginVisible(true);
			fComment->setText("A local astronomical system (LA) at P has his vertical axis in the same direction of the vertical of P and the Y axis oriented in the north direction. The geodetic reference system is the geoid CG2000 at the machine level. <a href = \"https://edms.cern.ch/ui/file/107906/0/georefsys.pdf\">  &Sigma;DMS</a>");
			refFrameChanged(TRefSystemFactory::ERefFrame::kLA2000Machine);
			break;
		case 5:
			setOriginVisible(true);
			fComment->setText("A local astronomical system (LA) at P has his vertical axis in the same direction of the vertical of P and the Y axis oriented in the north direction. The geodetic reference system is the geoid CG2000 at the topographic surface. <a href = \"https://edms.cern.ch/ui/file/107906/0/georefsys.pdf\">  &Sigma;DMS</a>");
			refFrameChanged(TRefSystemFactory::ERefFrame::kLA2000Topo);
			break;
		case 6: 
			setOriginVisible(false);
			fComment->setText("A local astronomical system (LA) at P0 has his vertical axis in the same direction of the vertical of P and the Y axis oriented in the north direction. <a href = \"https://edms.cern.ch/ui/file/107906/0/georefsys.pdf\">  &Sigma;DMS</a>");
			refFrameChanged(TRefSystemFactory::ERefFrame::kLAp0);
			break;
		}
		break;
	case 2: //MLA
		switch (index)
		{	
		case 0:
			setOriginVisible(true);
			fComment->setText("A modified local astronomical system(MLA) is a transformed LA (based on the sphere) by a Helmert transformation(describe by a bearing and slope angle). <a href = \"https://edms.cern.ch/ui/file/107906/0/georefsys.pdf\">  &Sigma;DMS</a>");
			refFrameChanged(TRefSystemFactory::ERefFrame::kMLASphere);
			break;
		case 1:
			setOriginVisible(true);
			fComment->setText("A modified local astronomical system(MLA) is a transformed LA (based on the geoid CG1985 at level 0) by a Helmert transformation(describe by a bearing and slope angle). <a href = \"https://edms.cern.ch/ui/file/107906/0/georefsys.pdf\">  &Sigma;DMS</a>");
			refFrameChanged(TRefSystemFactory::ERefFrame::kMLA1985H0);
			break;
		case 2:
			setOriginVisible(true);
			fComment->setText("A modified local astronomical system(MLA) is a transformed LA (based on the geoid CG1985 at the machine level) by a Helmert transformation(describe by a bearing and slope angle). <a href = \"https://edms.cern.ch/ui/file/107906/0/georefsys.pdf\">  &Sigma;DMS</a>");
			refFrameChanged(TRefSystemFactory::ERefFrame::kMLA1985Machine);
			break;
		case 3:
			setOriginVisible(true);
			fComment->setText("A modified local astronomical system(MLA) is a transformed LA (based on the geoid CG2000 at level 0) by a Helmert transformation(describe by a bearing and slope angle). <a href = \"https://edms.cern.ch/ui/file/107906/0/georefsys.pdf\">  &Sigma;DMS</a>");
			refFrameChanged(TRefSystemFactory::ERefFrame::kMLA2000H0);
			break;
		case 4:
			setOriginVisible(true);
			fComment->setText("A modified local astronomical system(MLA) is a transformed LA (based on the geoid CG2000 at the machine level) by a Helmert transformation(describe by a bearing and slope angle). <a href = \"https://edms.cern.ch/ui/file/107906/0/georefsys.pdf\">  &Sigma;DMS</a>");
			refFrameChanged(TRefSystemFactory::ERefFrame::kMLA2000Machine);
			break;
		case 5:
			setOriginVisible(true);
			fComment->setText("A modified local astronomical system(MLA) is a transformed LA (based on the geoid CG2000 at the topographic surface) by a Helmert transformation(describe by a bearing and slope angle). <a href = \"https://edms.cern.ch/ui/file/107906/0/georefsys.pdf\">  &Sigma;DMS</a>");
			refFrameChanged(TRefSystemFactory::ERefFrame::kMLA2000Topo);
			break;
		}
		break;
	}

}

void TFileSettingsPanel::sManageEllipsoid(const int &index)
{
	switch (rowIndexList)
	{
	case 12: // LG
		switch (index)
		{
		case 0:
			setOriginVisible(true);
			fComment->setText("The local geodetic (LG) system is topocentric and a right-handed Cartesian coordinate system. The geodetic reference is the sphere. <a href = \"https://edms.cern.ch/ui/file/107906/0/georefsys.pdf\">  &Sigma;DMS</a>");
			refFrameChanged(TRefSystemFactory::ERefFrame::kLGSphere);
			break;
		case 1:
			setOriginVisible(true);
			fComment->setText("The local geodetic (LG) system is topocentric and a right-handed Cartesian coordinate system. The geodetic reference is the ellipsoid GRS80. <a href = \"https://edms.cern.ch/ui/file/107906/0/georefsys.pdf\">  &Sigma;DMS</a>");
			refFrameChanged(TRefSystemFactory::ERefFrame::kLGGRS80);
			break;
		case 2: 
			fComment->setText("The local geodetic (LG) system is topocentric and a right-handed Cartesian coordinate system at P0. <a href = \"https://edms.cern.ch/ui/file/107906/0/georefsys.pdf\">  &Sigma;DMS</a>");
			setOriginVisible(false);
			refFrameChanged(TRefSystemFactory::ERefFrame::kLGp0);
			break;
		}
		break;
	case 13: //MLG
		switch (index)
		{
		case 0:
			setOriginVisible(true);
			fComment->setText("The MLG is an LG (based on the sphere) modified by a Helmert transformation (describe by a bearing and slope angle) <a href = \"https://edms.cern.ch/ui/file/107906/0/georefsys.pdf\">  &Sigma;DMS</a>");
			refFrameChanged(TRefSystemFactory::ERefFrame::kMLGSphere);
			break;
		case 1:
			setOriginVisible(true);
			fComment->setText("The MLG is an LG (base on the ellipsoid GRs80) modified by a Helmert transformation (describe by a bearing and slope angle) <a href = \"https://edms.cern.ch/ui/file/107906/0/georefsys.pdf\">  &Sigma;DMS</a>" ) ;
			refFrameChanged(TRefSystemFactory::ERefFrame::kMLGGRS80);
			break;
		}
		break;
	}

}

void TFileSettingsPanel::sManageCGRF(const int &)
{
	if (fCGRF->currentIndex() == 0)
	{
		fComment->setText("CERN geodetic reference frame based on the ellipsoidal reference surface");
		refFrameChanged(TRefSystemFactory::ERefFrame::kCGRF);
	}
	else if (fCGRF->currentIndex() == 1)
	{
		fComment->setText("CERN geodetic reference frame based on spherical reference surface");
		refFrameChanged(TRefSystemFactory::ERefFrame::kCGRFSphere);
	}
	else if (fCGRF->currentIndex() == 2)
	{
		fComment->setText("CERN geodetic reference frame based on the ellipsoidal reference surface, transverse mercator projection");
		refFrameChanged(TRefSystemFactory::ERefFrame::kCGRFMercator_eh);
	}
}

void TFileSettingsPanel::sManageSwissSystem(const int &index)
{
	if (rowIndexList == 8)
		switch (index)
		{
		case 0:
			fComment->setText("New local reference system for the Swiss national survey LV95. Based on the Bessel 1941 ellipsoid.");
			refFrameChanged(TRefSystemFactory::ERefFrame::kCH1903plus);
			break;
		case 1:
			fComment->setText("New global reference system for the Swiss national survey LV95. Based on ETRF93 at epoch 1993.0.");
			refFrameChanged(TRefSystemFactory::ERefFrame::kCHTRF95);
			break;
#ifdef USE_SWISSTOPO
		case 2: 
			fComment->setText("Triangulation network of 1903 (Bessel 1941 ellipsoidal height)");
			refFrameChanged(TRefSystemFactory::ERefFrame::kSwissLV03_eh);
			break;
		case 3:
			fComment->setText("Triangulation network of 1903 (LN02 leveled height)");
			refFrameChanged(TRefSystemFactory::ERefFrame::kSwissLV03_ln02);
			break;
		case 4:
			fComment->setText("Triangulation network of 1903 (LHN95 CHGeo 2004 orthometric height)");
			refFrameChanged(TRefSystemFactory::ERefFrame::kSwissLV03_lhn95);
			break;
		case 5:
			fComment->setText("New triangulation based on CHTRS95 (global positioning) and CH1903+ (local positioning). Ellipsoidal heights are relative to the Bessel 1941 ellispoid");
			refFrameChanged(TRefSystemFactory::ERefFrame::kSwissLV95_eh);
			break;
		case 6:
			fComment->setText("New triangulation based on CHTRS95 (global positioning) and CH1903+ (local positioning). LN02 leveled height");
			refFrameChanged(TRefSystemFactory::ERefFrame::kSwissLV95_ln02);
			break;
		case 7:
			fComment->setText("New triangulation based on CHTRS95 (global positioning) and CH1903+ (local positioning). CHGeo 2004 orthometric height");
			refFrameChanged(TRefSystemFactory::ERefFrame::kSwissLV95_lhn95);
			break;
#endif
		}
}

void TFileSettingsPanel::sManageFrenchSytem(const int &index)
{
	if (rowIndexList ==9)
		switch (index)
		{
		case 0:
			fComment->setText("Conique conforme projection of the RGF93. Ellipsoidal heights are relative to the ellispoid GRS80.");
			refFrameChanged(TRefSystemFactory::ERefFrame::kLambert93_eh);
			break;
		case 1:
			fComment->setText("Conique conforme projection of the RGF93 (altitude NGF-IGN69). The RAF20 altimetric conversion grid is used.");
			refFrameChanged(TRefSystemFactory::ERefFrame::kLambert93_ign69);
			break;
		case 2:
			fComment->setText("Geocentric system based on the GRS80 ellipsoid. It is based on ETRF2000 at epoch 2019.0.");
			refFrameChanged(TRefSystemFactory::ERefFrame::kRGF93);
			break;
		case 3: 
			fComment->setText("Projection and ellipsoidal height of the RGF93 to minimize the linear alteration (9 projections to define the French areas. The 5th (CC46) is relative to the Geneva latitude). Ellipsoidal heights are relative to the ellispoid GRS80.");
			refFrameChanged(TRefSystemFactory::ERefFrame::kFrenchRGF93_CC46_eh);
			break;
		case 4:
			fComment->setText("Projection and altitude NGF-IGN69 of the RGF93 to minimize the linear alteration (9 projections to define the French areas. The 5th (CC46) is relative to the Geneva latitude). The RAF20 altimetric conversion grid is used.");
			refFrameChanged(TRefSystemFactory::ERefFrame::kFrenchRGF93_CC46_ign69);
			break;
		}
	

}

void TFileSettingsPanel::sManageCernSystem(const int &index)
{
	if (rowIndexList == 3)
		switch (index)
	{
		case 0:
			fComment->setText("X and Y coordinates come from the CCS. The H coordinate represents the height of geoid CG2000 defined in 2000 at the ellipsoid (H=0) ");
			refFrameChanged(TRefSystemFactory::ERefFrame::kCernXYHg00);
			break;
		case 1:
			fComment->setText("X and Y coordinates come from the CCS. The H coordinate represents the height of geoid CG2000 defined in 2000 in the machine ");
			refFrameChanged(TRefSystemFactory::ERefFrame::kCernXYHg00Machine);
			break;
		case 2:
			fComment->setText("X and Y coordinates come from the CCS. The H coordinate represents the height of geoid CG2000 defined in 2000 on the top ");
			refFrameChanged(TRefSystemFactory::ERefFrame::kCernXYHg00Topo);
			break;
		case 3:
			fComment->setText("X and Y coordinates come from the CCS. The H coordinate represents the height of geoid CG1985 defined in 1985 on the top ");
			refFrameChanged(TRefSystemFactory::ERefFrame::kCernXYHg85);
			break;
		case 4:
			fComment->setText("X and Y coordinates come from the CCS. The H coordinate represents the height of geoid CG1985 defined in 1985 in the tunnel ");
			refFrameChanged(TRefSystemFactory::ERefFrame::kCernXYHg85Machine);
			break;
		case 5:
			fComment->setText("X and Y coordinates come from the CCS. The H coordinate represents the height of the sphere defined for the SPS ");
			refFrameChanged(TRefSystemFactory::ERefFrame::kCERNXYHsSphereSPS);
			break;
	}

}

void TFileSettingsPanel::setupUi()
{
	this->setWindowTitle("File configuration");
	this->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
	fComment->setFixedHeight(60);

	QVBoxLayout * l0 = new QVBoxLayout(this);
	QVBoxLayout * l0bis = new QVBoxLayout();
	QHBoxLayout * l1 = new QHBoxLayout();
	QVBoxLayout * l2 = new QVBoxLayout(); //origin
	QHBoxLayout * l3 = new QHBoxLayout(); //coo system + angle unit
	QHBoxLayout * l4 = new QHBoxLayout(); //precision
	QHBoxLayout * l5 = new QHBoxLayout(); // comments
	QHBoxLayout * l6 = new QHBoxLayout(); //combobox CGRF, GEOID or ELLIPSOID, ITRF, ETRF swiss, french or cern system
	QHBoxLayout * l7 = new QHBoxLayout(); //Epoch

	l0->addWidget(fComment);
	l0->addLayout(l1);
	l1->addLayout(l0bis);
	l0bis->addWidget(fRefFrameList);
	l1->addLayout(l2);
	l2->addLayout(l5);
	l2->addLayout(l6);
	l2->addLayout(l7);
	l2->addLayout(l3);
	l2->addLayout(l4);
	l2->addWidget(fOriginLoaderPanel);
	fOriginLoaderPanel->setVisible(false);
	l2->addWidget(fOriginEditor);
	fOriginEditor->setVisible(false);
	originGroup->addButton(fOriginEditor->ui.radioButton, kFromEditor);
	originGroup->addButton(fOriginLoaderPanel->ui.radioButton, kFromFile);

	l3->addWidget(fCooSystemLabel);
	l3->addWidget(fCooSystem);
	l3->addWidget(fAngleUnitLabel);
	l3->addWidget(fAngleUnit);
	if (fShowPrecisionSelector)
	{
		l4->addWidget(fPrecisionLabel);
		l4->addWidget(fPrecision);
	}
	else
	{
		fPrecisionLabel->setVisible(false);
		fPrecision->setVisible(false);
	}	
	l6->addWidget(fGeoidLabel);
	l6->addWidget(fGeoid);
	l6->addWidget(fEllipsoidLabel);
	l6->addWidget(fEllipsoid);
	l6->addWidget(fCGRFLabel);
	l6->addWidget(fCGRF);
	l6->addWidget(fSwissLabel);
	l6->addWidget(fSwiss);
	l6->addWidget(fFrenchLabel);
	l6->addWidget(fFrench);
	l6->addWidget(fXYHgLabel);
	l6->addWidget(fXYHg);
	l6->addWidget(fITRFSolutionLabel);
	l6->addWidget(fITRFSolution);
	l6->addWidget(fETRFSolutionLabel);
	l6->addWidget(fETRFSolution);
	l7->addWidget(fEpochLabel);
	l7->addWidget(fEpochEdit);

	//resize objetcts
	fComment->setAlignment(Qt::AlignTop);
	fCooSystemLabel->setMaximumHeight(20);
	fAngleUnitLabel->setMaximumHeight(20);
	fPrecisionLabel->setMaximumHeight(20);
	fCooSystem->setMaximumHeight(20);
	fAngleUnit->setMaximumHeight(20);
	fPrecision->setMaximumHeight(20);
	fCGRFLabel->setMaximumHeight(20);
	fCGRF->setMaximumHeight(20);
	fGeoidLabel->setMaximumHeight(20);
	fGeoid->setMaximumHeight(20);
	fEllipsoidLabel->setMaximumHeight(20);
	fEllipsoid->setMaximumHeight(20);
	fSwissLabel->setMaximumHeight(20);
	fSwiss->setMaximumHeight(20);
	fFrenchLabel->setMaximumHeight(20);
	fFrench->setMaximumHeight(20);
	fITRFSolutionLabel->setMaximumHeight(20);
	fITRFSolution->setMaximumHeight(20);
	fETRFSolutionLabel->setMaximumHeight(20);
	fETRFSolution->setMaximumHeight(20);
	fEpochLabel->setMaximumHeight(20);
	fEpochEdit->setMaximumHeight(20);
	fXYHgLabel->setMaximumHeight(20);
	fXYHg->setMaximumHeight(20);

	//if an hypertext is clicked, open the window in the navigator
	fComment->setOpenExternalLinks(true);
}

void TFileSettingsPanel::setupRefFrameList()
{
	fRefFrameModel = new QStandardItemModel;
	QStandardItem *rootNode = fRefFrameModel->invisibleRootItem();

	//root
	QStandardItem *CCSItem = new QStandardItem("CCS");
	QStandardItem *CGRFItem = new QStandardItem("CGRF");
	QStandardItem *MLAItem = new QStandardItem("MLA");
	QStandardItem *XYHgItem = new QStandardItem("CernXYHg");

	QStandardItem *OtherItem = new QStandardItem("Others");

	QStandardItem *ITRFItem = new QStandardItem("ITRF 97");
	QStandardItem *WGS84Item = new QStandardItem("WGS84 (G2139)");
	QStandardItem *ETRF93Item = new QStandardItem("ETRF93");
	QStandardItem* ETRF2Item = new QStandardItem("ETRF");
	QStandardItem* ITRF2Item = new QStandardItem("ITRF");
	QStandardItem *SwissItem = new QStandardItem("Swiss system");
	QStandardItem *FrenchItem = new QStandardItem("French system");
	QStandardItem *XYHeItem = new QStandardItem("CernXYHe");
	QStandardItem *X0Y0HeItem = new QStandardItem("CernX0Y0He");
	QStandardItem *LGItem = new QStandardItem("LG");
	QStandardItem *MLGItem = new QStandardItem("MLG");
	QStandardItem *LAItem = new QStandardItem("LA");

	//building up the hierarchy
	rootNode->appendRow(CCSItem);
	rootNode->appendRow(CGRFItem);
	rootNode->appendRow(MLAItem);
	rootNode->appendRow(XYHgItem);

	rootNode->appendRow(OtherItem);

	rootNode->appendRow(ITRFItem);
	rootNode->appendRow(WGS84Item);
	rootNode->appendRow(ETRF93Item);
	rootNode->appendRow(SwissItem);
	rootNode->appendRow(FrenchItem);
	rootNode->appendRow(XYHeItem);
	rootNode->appendRow(X0Y0HeItem);
	rootNode->appendRow(LGItem);
	rootNode->appendRow(MLGItem);
	rootNode->appendRow(LAItem);
	rootNode->appendRow(ITRF2Item);
	rootNode->appendRow(ETRF2Item);

	//register the model
	fRefFrameList->setModel(fRefFrameModel);

	//geometry
	fRefFrameList->setFixedWidth(100);
	fRefFrameList->setMaximumHeight(190);
	for (int i = 5; i < 18; i++)
		fRefFrameList->setRowHidden(i,true);

	//set not editable
	CCSItem->setEditable(false);
	CGRFItem->setEditable(false);
	MLAItem->setEditable(false);
	XYHgItem->setEditable(false);
	OtherItem->setEditable(false);
	ITRFItem->setEditable(false);
	WGS84Item->setEditable(false);
	ETRF93Item->setEditable(false);
	ETRF2Item->setEditable(false);
	ITRF2Item->setEditable(false);
	SwissItem->setEditable(false);
	FrenchItem->setEditable(false);
	XYHeItem->setEditable(false);
	X0Y0HeItem->setEditable(false);
	LGItem->setEditable(false);
	MLGItem->setEditable(false);
	LAItem->setEditable(false);

	//default reference frame
	fRefFrameList->setCurrentIndex(fRefFrameModel->index(0,0));
	manageRefFrame(fRefFrameModel->index(0, 0));
}

void TFileSettingsPanel::setupComboBox()
{
	fCGRFLabel->setText("Reference surface:");
	fCGRFLabel->setVisible(false);
	fCGRF->addItem("Ellipsoid", TRefSystemFactory::ERefFrame::kCGRF);
	fCGRF->addItem("Sphere", TRefSystemFactory::ERefFrame::kCGRFSphere);
	fCGRF->addItem("Transverse Mercator", TRefSystemFactory::ERefFrame::kCGRFMercator_eh);
	fCGRF->setVisible(false);

	fGeoidLabel->setText("Reference geoid:");
	fGeoidLabel->setVisible(false);
	fGeoid->addItem("Sphere", TRefSystemFactory::EGeoid::kCGSphere);
	fGeoid->addItem("1985", TRefSystemFactory::EGeoid::kCG1985);
	fGeoid->addItem("1985 machine", TRefSystemFactory::EGeoid::kCG1985Machine);
	fGeoid->addItem("2000", TRefSystemFactory::EGeoid::kCG2000);
	fGeoid->addItem("2000 machine", TRefSystemFactory::EGeoid::kCG2000Machine);
	fGeoid->addItem("2000 topo", TRefSystemFactory::EGeoid::kCG2000topo);
	fGeoid->addItem("P0", 4);
	fGeoid->setVisible(false);
	fGeoid->setCurrentIndex(2);

	fEllipsoidLabel->setText("Reference ellipsoid:");
	fEllipsoidLabel->setVisible(false);
	fEllipsoid->addItem("Sphere", TRefSystemFactory::ERefEll::kSphere);
	fEllipsoid->addItem("GRS80", TRefSystemFactory::ERefEll::kGRS80);
	fEllipsoid->addItem("P0", 4);
	fEllipsoid->setVisible(false); 
	fEllipsoid->setCurrentIndex(1);

	fSwissLabel->setText("Swiss system:");
	fSwissLabel->setVisible(false);
	fSwiss->addItem("CH1903+", TRefSystemFactory::ERefFrame::kCH1903plus);
	fSwiss->addItem("CHTRF95", TRefSystemFactory::ERefFrame::kCHTRF95);
#ifdef USE_SWISSTOPO
	fSwiss->addItem("LV03 (ellipsoidal height)", TRefSystemFactory::ERefFrame::kSwissLV03_eh);
	fSwiss->addItem("LV03 (LN02 leveled height)", TRefSystemFactory::ERefFrame::kSwissLV03_ln02);
	fSwiss->addItem("LV03 (LHN95 orthometric height)", TRefSystemFactory::ERefFrame::kSwissLV03_lhn95);
	fSwiss->addItem("LV95 (ellipsoidal height)", TRefSystemFactory::ERefFrame::kSwissLV95_eh);
	fSwiss->addItem("LV95 (LN02 leveled height)", TRefSystemFactory::ERefFrame::kSwissLV95_ln02);
	fSwiss->addItem("LV95 (LHN95 orthometric height)", TRefSystemFactory::ERefFrame::kSwissLV95_lhn95);
#endif
	fSwiss->setVisible(false);

	fFrenchLabel->setText("French system:");
	fFrenchLabel->setVisible(false);
	fFrench->addItem("Lambert93 (ellipsoidal height)", TRefSystemFactory::ERefFrame::kLambert93_eh);
	fFrench->addItem("Lambert93 (NGF-IGN69)", TRefSystemFactory::ERefFrame::kLambert93_ign69);
	fFrench->addItem("RGF93 v2b", TRefSystemFactory::ERefFrame::kRGF93);
	fFrench->addItem("RGF93 CC46 (ellipsoidal height)", TRefSystemFactory::ERefFrame::kFrenchRGF93_CC46_eh);
	fFrench->addItem("RGF93 CC46 (NGF-IGN69)", TRefSystemFactory::ERefFrame::kFrenchRGF93_CC46_ign69);
	fFrench->setVisible(false);

	fITRFSolutionLabel->setText("Solution:");
	fITRFSolutionLabel->setVisible(false);
	fITRFSolution->addItem("ITRF 88", TRefSystemFactory::ERefFrame::kITRFin);
	fITRFSolution->addItem("ITRF 89", TRefSystemFactory::ERefFrame::kITRFin);
	fITRFSolution->addItem("ITRF 90", TRefSystemFactory::ERefFrame::kITRFin);
	fITRFSolution->addItem("ITRF 91", TRefSystemFactory::ERefFrame::kITRFin);
	fITRFSolution->addItem("ITRF 92", TRefSystemFactory::ERefFrame::kITRFin);
	fITRFSolution->addItem("ITRF 93", TRefSystemFactory::ERefFrame::kITRFin);
	fITRFSolution->addItem("ITRF 94", TRefSystemFactory::ERefFrame::kITRFin);
	fITRFSolution->addItem("ITRF 96", TRefSystemFactory::ERefFrame::kITRFin);
	fITRFSolution->addItem("ITRF 97", TRefSystemFactory::ERefFrame::kITRFin);
	fITRFSolution->addItem("ITRF 2000", TRefSystemFactory::ERefFrame::kITRFin);
	fITRFSolution->addItem("ITRF 2005", TRefSystemFactory::ERefFrame::kITRFin);
	fITRFSolution->addItem("ITRF 2008", TRefSystemFactory::ERefFrame::kITRFin);
	fITRFSolution->addItem("ITRF 2014", TRefSystemFactory::ERefFrame::kITRFin);
	fITRFSolution->addItem("ITRF 2020", TRefSystemFactory::ERefFrame::kITRFin);
	fITRFSolution->setVisible(false);

	fETRFSolutionLabel->setText("Solution:");
	fETRFSolutionLabel->setVisible(false);
	fETRFSolution->addItem("ETRF 89", TRefSystemFactory::ERefFrame::kETRFin);
	fETRFSolution->addItem("ETRF 90", TRefSystemFactory::ERefFrame::kETRFin);
	fETRFSolution->addItem("ETRF 91", TRefSystemFactory::ERefFrame::kETRFin);
	fETRFSolution->addItem("ETRF 92", TRefSystemFactory::ERefFrame::kETRFin);
	fETRFSolution->addItem("ETRF 93", TRefSystemFactory::ERefFrame::kETRFin);
	fETRFSolution->addItem("ETRF 94", TRefSystemFactory::ERefFrame::kETRFin);
	fETRFSolution->addItem("ETRF 96", TRefSystemFactory::ERefFrame::kETRFin);
	fETRFSolution->addItem("ETRF 97", TRefSystemFactory::ERefFrame::kETRFin);
	fETRFSolution->addItem("ETRF 2000", TRefSystemFactory::ERefFrame::kETRFin);
	fETRFSolution->addItem("ETRF 2005", TRefSystemFactory::ERefFrame::kETRFin);
	fETRFSolution->addItem("ETRF 2014", TRefSystemFactory::ERefFrame::kETRFin);
	fETRFSolution->setVisible(false);

	fEpochLabel->setText("Epoch (decimal year):");
	fEpochLabel->setVisible(false);
	fEpochEdit->setSpecialValueText("Epoch (yyyy.yy)");
	fEpochEdit->setButtonSymbols(QAbstractSpinBox::NoButtons);
	fEpochEdit->setVisible(false);
	fEpochEdit->setMaximumWidth(120);
	fEpochEdit->setRange(0,3000.0);
	fEpochEdit->setDecimals(3);

	fXYHgLabel->setText("XYHg system:");
	fXYHgLabel->setVisible(false);
	fXYHg->addItem("CERN XYHg00", TRefSystemFactory::ERefFrame::kCernXYHg00);
	fXYHg->addItem("CERN XYHg (RS2K)", TRefSystemFactory::ERefFrame::kCernXYHg00Machine);
	fXYHg->addItem("CERN XYHg2000 (topo)", TRefSystemFactory::ERefFrame::kCernXYHg00Topo);
	fXYHg->addItem("CERN XYHg1985 (topo)", TRefSystemFactory::ERefFrame::kCernXYHg85);
	fXYHg->addItem("CERN XYHg (LHC)", TRefSystemFactory::ERefFrame::kCernXYHg85Machine);
	fXYHg->addItem("CERN XYHs (SPS)", TRefSystemFactory::ERefFrame::kCERNXYHsSphereSPS);
	fXYHg->setVisible(false);
	fXYHg->setCurrentIndex(4);

	
	fAngleUnitLabel->setText("Angle Unit:");
	fAngleUnitLabel->setVisible(false);
	fAngleUnit->addItem("Gons", TAngle::kGons);
	fAngleUnit->addItem("Decimal Degrees", TAngle::kDeciDegs);
	fAngleUnit->addItem("DMS", TAngle::kDMS);
	fAngleUnit->setVisible(false);

	fCooSystemLabel->setText("Coordinate system:");
	fCooSystemLabel->setVisible(true);

	fPrecisionLabel->setText("Precision:");
	fPrecisionLabel->setVisible(true);
	for (int i = 0; i <= 7; ++i)
		fPrecision->addItem(tr("%1").arg(i), i);
	fPrecision->setCurrentIndex(5);
}

//ANGLE UNIT
TAngle::EUnits TFileSettingsPanel::getUnits() const
{
	if (fAngleUnit->currentIndex() == 0) //gon
		return TAngle::kGons;
	else if (fAngleUnit->currentIndex() == 1)
		return TAngle::kDeciDegs;
	else if (fAngleUnit->currentIndex() == 2)
		return TAngle::kDMS;

	throw std::logic_error("Angular units should be selected");
}

void TFileSettingsPanel::setCoordSys(TCoordSysFactory::ECoordSys system)
{
	bool state = (system == TCoordSysFactory::kGeodetic);

	fAngleUnitLabel->setVisible(state);
	fAngleUnit->setVisible(state);
	fDataParams->setCoordSys(getCoordSys());
	fDataParams->setAngUnits(getUnits());
}

//COORDINATE SYSTEM
void TFileSettingsPanel::setCoordinateSystem(TRefSystemFactory::ERefFrame frame)
{

	if (TRefFrameInfo::isCoordSysAllowed(frame, TCoordSysFactory::k3DCartesian))
	{
		if (fCooSystem->findData(TCoordSysFactory::k3DCartesian) == -1)
			fCooSystem->addItem("Cartesian", TCoordSysFactory::k3DCartesian);
	}
	else
	{
		if (fCooSystem->findData(TCoordSysFactory::k3DCartesian) != -1)
			fCooSystem->removeItem(fCooSystem->findData(TCoordSysFactory::k3DCartesian));
	}

	if (TRefFrameInfo::isCoordSysAllowed(frame, TCoordSysFactory::kGeodetic))
	{
		if (fCooSystem->findData(TCoordSysFactory::kGeodetic) == -1)
			fCooSystem->addItem("Geodetic", TCoordSysFactory::kGeodetic);
	}
	else
	{
		if (fCooSystem->findData(TCoordSysFactory::kGeodetic) != -1)
			fCooSystem->removeItem(fCooSystem->findData(TCoordSysFactory::kGeodetic));
	}

	if (TRefFrameInfo::isCoordSysAllowed(frame, TCoordSysFactory::k2DPlusH))
	{
		if (fCooSystem->findData(TCoordSysFactory::k2DPlusH) == -1)
			fCooSystem->addItem("2D + H", TCoordSysFactory::k2DPlusH);
	}
	else
	{
		if (fCooSystem->findData(TCoordSysFactory::k2DPlusH) != -1)
			fCooSystem->removeItem(fCooSystem->findData(TCoordSysFactory::k2DPlusH));
	}
}

void TFileSettingsPanel::resetCoordinateSystem()
{
	if (fCooSystem->findData(TCoordSysFactory::k2DPlusH) != -1)
		fCooSystem->removeItem(fCooSystem->findData(TCoordSysFactory::k2DPlusH));
	if (fCooSystem->findData(TCoordSysFactory::kGeodetic) != -1)
		fCooSystem->removeItem(fCooSystem->findData(TCoordSysFactory::kGeodetic));
	if (fCooSystem->findData(TCoordSysFactory::k3DCartesian) != -1)
		fCooSystem->removeItem(fCooSystem->findData(TCoordSysFactory::k3DCartesian));

}

TCoordSysFactory::ECoordSys TFileSettingsPanel::getCoordSys() const
{
	if (fCooSystem->currentText() == "Cartesian")
		return TCoordSysFactory::k3DCartesian;
	else if (fCooSystem->currentText() == "Geodetic")
		return TCoordSysFactory::kGeodetic;
	else if (fCooSystem->currentText() == "2D + H")
		return TCoordSysFactory::k2DPlusH;
	else //default value
		return TCoordSysFactory::k3DCartesian;
}

void TFileSettingsPanel::sCoordSysClicked(int)
{
	setCoordSys(getCoordSys());
	fDataParams->setCoordSys(getCoordSys());
	fDataParams->setAngUnits(getUnits());
	sPrecisionChanged(fPrecision->currentData().toInt());

	emit SIG_dataParameterChanged(fDataParams, input);
}

//PRECISION
void TFileSettingsPanel::sPrecisionChanged(int index)
{
	if (fShowPrecisionSelector)
		fDataParams->setPrecision(fPrecision->itemData(index).toInt());

	emit SIG_dataParameterChanged(fDataParams, input);
}

//EPOCH
void TFileSettingsPanel::sEpochChanged(const double& coordEpoch)
{
	if (!coordEpoch==fEpochEdit->minimum())
	{
		fDataParams->setCoordEpoch(coordEpoch);
		TRefSystemFactory::getRefSystemFactory()->getTerrRefFrame(fDataParams->getRefFrameEnumerator())->setEpoch(coordEpoch);
		emit SIG_dataParameterChanged(fDataParams, input);
		return;
	}
	fDataParams->setCoordEpoch(NO_VALf); // when line edit is emptied
	return;
}

//ITRF and ETRF Solution
void TFileSettingsPanel::sITRFChanged(const int&)
{
	fDataParams->setSolution(fITRFSolution->currentText().toStdString());
	TRefSystemFactory::getRefSystemFactory()->getTerrRefFrame(fDataParams->getRefFrameEnumerator())->setSolution(fITRFSolution->currentText().toStdString());	
	emit SIG_dataParameterChanged(fDataParams, input);

}

void TFileSettingsPanel::sETRFChanged(const int&)
{
	fDataParams->setSolution(fETRFSolution->currentText().toStdString());
	TRefSystemFactory::getRefSystemFactory()->getTerrRefFrame(fDataParams->getRefFrameEnumerator())->setSolution(fETRFSolution->currentText().toStdString());
	emit SIG_dataParameterChanged(fDataParams, input);

}

//ORIGIN
void TFileSettingsPanel::sOriginClicked(int)
{
	if (fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLA1985H0 ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLA1985Machine ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLA2000H0 ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLA2000Machine ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLA2000Topo ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLASphere ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLGGRS80 ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLGSphere ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLA2000H0 ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLA2000Machine ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLA2000Topo ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLA1985H0 ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLA1985Machine ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLASphere ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLGGRS80 ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLGSphere) 
	{
		sOriginChanged();
	}
}

void TFileSettingsPanel::sOriginChanged()
{
	if (originGroup->checkedId() == 1) //fOriginLoaderPanel selected
	{
		if (fOriginLoaderPanel->originAvailable())
		{
			fDataParams->setLocalSystemOrigin(fOriginLoaderPanel->getOrigin());
			fDataParams->setOriginFile(fOriginLoaderPanel->getFileName());
		}
		else
		{
			fDataParams->setLocalSystemOrigin(nullptr);
			fDataParams->setOriginFile("");
		}
		fOriginEditor->loadOrigin(fDataParams);

		emit SIG_dataParameterChanged(fDataParams, input);
		return;
	}
	else //fOriginEditor selected
	{
		//not all values have been fill in
		if (!fOriginEditor->isComplete(fDataParams->getRefFrameEnumerator()))
		{
			//lso need to be reset to nullptr because if we modified the origineditor deleting one value, the lso is already initialized
			fDataParams->setLocalSystemOrigin(nullptr);
		}
		else
			fDataParams->setLocalSystemOrigin(fOriginEditor->getOrigin());
		if (fOriginLoaderPanel->getOrigin() != fOriginEditor->getOrigin())
			fDataParams->setOriginFile("");
		fOriginLoaderPanel->loadOrigin(fDataParams);

		emit SIG_dataParameterChanged(fDataParams, input);
		return;
	}
}

void TFileSettingsPanel::setOriginVisible(bool state)
{
	fOriginFileLoaderVisible = state;
	fOriginLoaderPanel->setVisible(state);
	fOriginEditor->setVisible(state);
	
	//default radiobuttom clicked
	fOriginEditor->ui.radioButton->setChecked(true);
	sOriginClicked(kFromEditor);
}


//DATAPARAMETER
void TFileSettingsPanel::setDataParameters(std::shared_ptr<TDataParameters> params)
{
	if (params == fDataParams) // guard against loop
		return;

	fDataParams.reset();
	fDataParams = params;
	hideRFList(fDataParams->getRefFrameEnumerator());
	updateViewFromDataSetParameters(fDataParams);
}

//UPDATE VIEW
void TFileSettingsPanel::updateViewFromDataSetParameters(std::shared_ptr<TDataParameters> params)
{
	//updata TDataParameters
	setDataParameters(params);

	//update Precision view
	fPrecision->setCurrentIndex(params->getCoordPrecision());

	//set or reset origin value
	fOriginEditor->loadOrigin(params);
	fOriginLoaderPanel->loadOrigin(params);

	//reset epoch and solution 
	fEpochEdit->specialValueText();

	if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kCCS)
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(0, 0));
	//CGRF
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kCGRF)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(1, 0));
		fCGRF->setCurrentIndex(0);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kCGRFSphere)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(1, 0));
		fCGRF->setCurrentIndex(1);
	}
	//MLA
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLASphere)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(2, 0));
		fGeoid->setCurrentIndex(0);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLA1985H0)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(2, 0));
		fGeoid->setCurrentIndex(1);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLA1985Machine)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(2, 0));
		fGeoid->setCurrentIndex(2);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLA2000H0)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(2, 0));
		fGeoid->setCurrentIndex(3);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLA2000Machine)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(2, 0));
		fGeoid->setCurrentIndex(4);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLA2000Topo)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(2, 0));
		fGeoid->setCurrentIndex(5);
	}
	//XYH
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kCernXYHg00)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(3, 0));
		fXYHg->setCurrentIndex(0);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kCernXYHg00Machine)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(3, 0));
		fXYHg->setCurrentIndex(1);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kCernXYHg00Topo)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(3, 0));
		fXYHg->setCurrentIndex(2);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kCernXYHg85)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(3, 0));
		fXYHg->setCurrentIndex(3);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kCernXYHg85Machine)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(3, 0));
		fXYHg->setCurrentIndex(4);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kCERNXYHsSphereSPS)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(3, 0));
		fXYHg->setCurrentIndex(5);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kITRF97)
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(5, 0));
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kWGS84_G2139)
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(6, 0));
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kETRF93)
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(7, 0));



	//swiss
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kCH1903plus)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(8, 0));
		fSwiss->setCurrentIndex(0);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kCHTRF95)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(8, 0));
		fSwiss->setCurrentIndex(1);
	}
#ifdef USE_SWISSTOPO
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kSwissLV03_eh)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(8, 0));
		fSwiss->setCurrentIndex(2);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kSwissLV03_ln02)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(8, 0));
		fSwiss->setCurrentIndex(3);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kSwissLV03_lhn95)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(8, 0));
		fSwiss->setCurrentIndex(4);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kSwissLV95_eh)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(8, 0));
		fSwiss->setCurrentIndex(5);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kSwissLV95_ln02)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(8, 0));
		fSwiss->setCurrentIndex(6);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kSwissLV95_lhn95)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(8, 0));
		fSwiss->setCurrentIndex(7);
	}
#endif
	//french
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLambert93_eh)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(9, 0));
		fFrench->setCurrentIndex(0);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLambert93_ign69)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(9, 0));
		fFrench->setCurrentIndex(1);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kRGF93)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(9, 0));
		fFrench->setCurrentIndex(2);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kFrenchRGF93_CC46_eh)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(9, 0));
		fFrench->setCurrentIndex(3);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kFrenchRGF93_CC46_ign69)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(9, 0));
		fFrench->setCurrentIndex(4);
	}
	else if (params->getRefFrameEnumerator() == (TRefSystemFactory::ERefFrame::kCernXYHe))
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(10, 0));
	else if (params->getRefFrameEnumerator() == (TRefSystemFactory::ERefFrame::kCernX0Y0He))
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(11, 0));
	//LG
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLGSphere)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(12, 0));
		fEllipsoid->setCurrentIndex(0);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLGGRS80)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(12, 0));
		fEllipsoid->setCurrentIndex(1);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLGp0)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(12, 0));
		fEllipsoid->setCurrentIndex(2);
	}
	//MLG
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLGSphere)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(13, 0));
		fEllipsoid->setCurrentIndex(0);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLGGRS80)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(13, 0));
		fEllipsoid->setCurrentIndex(1);
	}
	//LA
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLAp0)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(14, 0));
		fGeoid->setCurrentIndex(6);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLASphere)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(14, 0));
		fGeoid->setCurrentIndex(0);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLA1985H0)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(14, 0));
		fGeoid->setCurrentIndex(1);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLA1985Machine)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(14, 0));
		fGeoid->setCurrentIndex(2);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLA2000H0)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(14, 0));
		fGeoid->setCurrentIndex(3);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLA2000Machine)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(14, 0));
		fGeoid->setCurrentIndex(4);
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLA2000Topo)
	{
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(14, 0));
		fGeoid->setCurrentIndex(5);
	}

	//TRF (ITRF or ETRF solution)
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kITRFin)
	{
		std::string currentSolution = params->getSolution();
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(15, 0));
		fITRFSolution->setCurrentIndex(fITRFSolution->findText(QString::fromStdString(currentSolution)));
		fEpochEdit->setValue(fDataParams->getCoordEpoch());
	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kITRFout)
	{
		std::string currentSolution = params->getSolution();
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(15, 0));
		fITRFSolution->setCurrentIndex(fITRFSolution->findText(QString::fromStdString(currentSolution)));
		fEpochEdit->setValue(fDataParams->getCoordEpoch());

	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kETRFin)
	{
		std::string currentSolution = params->getSolution();
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(16, 0));
		fETRFSolution->setCurrentIndex(fETRFSolution->findText(QString::fromStdString(currentSolution)));
		fEpochEdit->setValue(fDataParams->getCoordEpoch());

	}
	else if (params->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kETRFout)
	{
		std::string currentSolution = params->getSolution();
		fRefFrameList->setCurrentIndex(fRefFrameModel->index(16, 0));
		fETRFSolution->setCurrentIndex(fETRFSolution->findText(QString::fromStdString(currentSolution)));
		fEpochEdit->setValue(fDataParams->getCoordEpoch());
	}
	//update coosystemview
	if (params->getCoordinateSystem() == TCoordSysFactory::ECoordSys::k3DCartesian)
	{
		fCooSystem->setCurrentText("Cartesian");
	}
	else if (params->getCoordinateSystem() == TCoordSysFactory::ECoordSys::kGeodetic)
	{
		if (params->getAngleUnits() == TAngle::EUnits::kGons)
		{
			fAngleUnit->setCurrentIndex(0);
		}
		else if (params->getAngleUnits() == TAngle::EUnits::kDeciDegs)
		{
			fAngleUnit->setCurrentIndex(1);
		}
		else if (params->getAngleUnits() == TAngle::EUnits::kDMS)
		{
			fAngleUnit->setCurrentIndex(2);
		}
		fCooSystem->setCurrentText("Geodetic");
	}
	else if (params->getCoordinateSystem() == TCoordSysFactory::ECoordSys::k2DPlusH)
	{
		fCooSystem->setCurrentText("2D + H");
	}
}

void TFileSettingsPanel::setEnable(bool enable)
{
	fRefFrameList->setEnabled(enable);
	fPrecisionLabel->setEnabled(enable);
	fPrecision->setEnabled(enable);
	fCooSystem->setEnabled(enable);
	fCooSystemLabel->setEnabled(enable);
	fOriginLoaderPanel->setEnabled(enable);
	fOriginEditor->setEnabled(enable);
	fAngleUnit->setEnabled(enable);
	fAngleUnitLabel->setEnabled(enable);
	fComment->setEnabled(enable);
	fGeoidLabel->setEnabled(enable);
	fGeoid->setEnabled(enable);
	fEllipsoidLabel->setEnabled(enable);
	fEllipsoid->setEnabled(enable);
	fCGRFLabel->setEnabled(enable);
	fCGRF->setEnabled(enable);
	fSwissLabel->setEnabled(enable);
	fSwiss->setEnabled(enable);
	fFrenchLabel->setEnabled(enable);
	fFrench->setEnabled(enable);
	fXYHgLabel->setEnabled(enable);
	fXYHg->setEnabled(enable);
	fITRFSolutionLabel->setEnabled(enable);
	fITRFSolution->setEnabled(enable);
	fETRFSolutionLabel->setEnabled(enable);
	fETRFSolution->setEnabled(enable);
	fEpochEdit->setEnabled(enable);
	fEpochLabel->setEnabled(enable);
}

void TFileSettingsPanel::hideRFList(TRefSystemFactory::ERefFrame frame)
{
	if (frame == TRefSystemFactory::ERefFrame::kCCS ||
		frame == TRefSystemFactory::ERefFrame::kCGRF || 
		frame == TRefSystemFactory::ERefFrame::kCGRFSphere || 
		frame == TRefSystemFactory::ERefFrame::kMLA1985H0 ||
		frame == TRefSystemFactory::ERefFrame::kMLA1985Machine ||
		frame == TRefSystemFactory::ERefFrame::kMLA2000H0 || 
		frame == TRefSystemFactory::ERefFrame::kMLA2000Machine ||
		frame == TRefSystemFactory::ERefFrame::kMLA2000Topo ||
		frame == TRefSystemFactory::ERefFrame::kMLASphere ||
		frame == TRefSystemFactory::ERefFrame::kCernXYHg00 || 
		frame == TRefSystemFactory::ERefFrame::kCernXYHg00Machine || 
		frame == TRefSystemFactory::ERefFrame::kCernXYHg00Topo ||  
		frame == TRefSystemFactory::ERefFrame::kCernXYHg85 || 
		frame == TRefSystemFactory::ERefFrame::kCernXYHg85Machine || 
		frame == TRefSystemFactory::ERefFrame::kCERNXYHsSphereSPS)
		for (int i = 5; i < 18; i++)
			fRefFrameList->setRowHidden(i, true);
	else
		for (int i = 5; i < 18; i++)
			fRefFrameList->setRowHidden(i, false);
}

//CLEAN VIEW
void TFileSettingsPanel::cleanPanel(TDataParameters fData)
{
	fDataParams.reset();
	fDataParams = std::make_shared<TDataParameters>(fData);
	fOriginEditor->loadOrigin(fDataParams);
	fOriginLoaderPanel->loadOrigin(fDataParams);
	fEpochEdit->minimum();
}

bool TFileSettingsPanel::isReferenceFrameConsistent()
{
	if (fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLA2000H0 ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLA2000Machine ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLA2000Topo ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLA1985H0 ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLA1985Machine ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLASphere ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLA1985H0 ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLA1985Machine ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLA2000H0 ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLA2000Machine ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLA2000Topo ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLASphere ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLGGRS80 ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kMLGSphere ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLGGRS80 ||
		fDataParams->getRefFrameEnumerator() == TRefSystemFactory::ERefFrame::kLGSphere)
	{
		if(!fDataParams->getLocalSystemOrigin())
			return false;
	}

	return true;
}
