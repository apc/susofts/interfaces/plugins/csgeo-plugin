/*!
	OriginLoaderPanel.h

	Widget class responsible of the origin widgets
*/

#ifndef TORIGIN_LODADER_PANEL_e3f753e67eff448ba31e6273087c7684
#define TORIGIN_LODADER_PANEL_e3f753e67eff448ba31e6273087c7684

#include <ui_OriginLoaderPanel.h>

#include <TLocalSystemOrigin.h>
#include <TDataParameters.h>

class OriginLoaderPanel : public QWidget
{
	Q_OBJECT
public:
	OriginLoaderPanel(QWidget * parent);
	TLocalSystemOrigin getOrigin() const { return TLocalSystemOrigin(fOrigin, fGisement, fSlope, fName); }
	Ui::OriginLoaderPanel ui;

	// return if the origin is set
	bool originAvailable() const{ return isOriginSet; };
	//read the origin file
	void readOrigin();
	// Allows to set the origin in the UI when we load a file
	bool loadOrigin(std::shared_ptr<TDataParameters> fData);
	std::string getFileName() const { return ui.filePath->text().toStdString(); }

signals:
	// signal emitted when the origin has been changed
	void SIG_OriginChanged();

private slots:
	void sBrowseButton();

private:
	bool isOriginSet;
	TSpatialPosition fOrigin;
	TAngle fGisement;
	TAngle fSlope;
	std::string fName;
};

#endif
