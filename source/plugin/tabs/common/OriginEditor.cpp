#include <stdexcept>
#include <string>
#include <Logger.hpp>


#include <QDoubleValidator>
#include <QRegExp>
#include <QRegExpValidator>
#include <QClipboard>

#include <TLocalSystemOrigin.h>
#include <TRefFrameInfo.h>
#include <TSpatialPosition.h>
#include "tabs/common/OriginEditor.hpp"
#include <CSGeoCore/headers/io/input_point/TOriginReader.h>

OriginEditor::OriginEditor(QWidget *parent) :
	QWidget(parent),
	fName("origin"),
	fOrigin(TSpatialPosition(TRefFrameInfo::getReferenceFrame(TRefSystemFactory::kCCS), 0.0, 0.0, 0.0, TCoordSysFactory::k3DCartesian)),
	fGisement(0.0),
	fSlope(0.0)
{
	rotationEnabled = false;
	ui.setupUi(this);

	ui.originName->setPlaceholderText("name");
	ui.originName->setFocus();
	ui.originX->setSpecialValueText("X [m]");
	ui.originX->setButtonSymbols(QAbstractSpinBox::NoButtons);
	ui.originX->setRange(-999999999.0, 999999999.0);
	ui.originX->setValue(ui.originX->minimum());
	ui.originX->setDecimals(6);
	ui.originX->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	ui.originX->setMinimumWidth(50);
	ui.originY->setSpecialValueText("Y [m]");
	ui.originY->setButtonSymbols(QAbstractSpinBox::NoButtons);
	ui.originY->setRange(-999999999.0, 999999999.0);
	ui.originY->setValue(ui.originY->minimum());
	ui.originY->setDecimals(6);
	ui.originY->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	ui.originY->setMinimumWidth(50);
	ui.originZ->setSpecialValueText("Z [m]");
	ui.originZ->setButtonSymbols(QAbstractSpinBox::NoButtons);
	ui.originZ->setRange(-999999999.0, 999999999.0);
	ui.originZ->setValue(ui.originZ->minimum());
	ui.originZ->setDecimals(6);
	ui.originZ->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	ui.originZ->setMinimumWidth(50);
	ui.originBearing->setSpecialValueText("Bearing [gon]");
	ui.originBearing->setButtonSymbols(QAbstractSpinBox::NoButtons);
	ui.originBearing->setRange(-400, 400);
	ui.originBearing->setValue(ui.originBearing->minimum());
	ui.originBearing->setDecimals(15);
	ui.originBearing->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	ui.originBearing->setMinimumWidth(50);
	ui.originSlope->setSpecialValueText("Slope [rad]");
	ui.originSlope->setButtonSymbols(QAbstractSpinBox::NoButtons);
	ui.originSlope->setRange(-3.15, 3.15);
	ui.originSlope->setValue(ui.originBearing->minimum());
	ui.originSlope->setDecimals(15);
	ui.originSlope->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	ui.originSlope->setMinimumWidth(50);
	
	connect(ui.originName, SIGNAL(textChanged(const QString &)), this, SLOT(sNameChanged(const QString &)));
	connect(ui.originX, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &OriginEditor::sXChanged);
	connect(ui.originY, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &OriginEditor::sYChanged);
	connect(ui.originZ, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &OriginEditor::sZChanged);
	connect(ui.originBearing, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &OriginEditor::sBearingChanged);
	connect(ui.originSlope, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &OriginEditor::sSlopeChanged);
}

void OriginEditor::setBearing(bool enable)
{
	rotationEnabled = enable;
	ui.originBearing->setVisible(enable);
	ui.originSlope->setVisible(enable);

	// reset slope and bearing if LA and LG used (not used)
	if (!enable)
	{
		fSlope = TAngle(0.0);
		fGisement = TAngle(0.0);
	}
}

void OriginEditor::sNameChanged(const QString &originName)
{
	ui.originName->blockSignals(true);
	//Manage copy/paste into name field and populate other fields
	if (QApplication::clipboard()->text() == originName)
	{
		QString origin;
		if (originName.trimmed().startsWith("ORIGIN"))
		{
			origin = QApplication::clipboard()->text();
		}
		else
		{
			origin = "ORIGIN  ";
			origin.append(QApplication::clipboard()->text());
		}
		TOriginReader originReader;
		try
		{
			TLocalSystemOrigin ori = originReader.readOrigin(origin.toStdString());
			ui.originName->setText(QString::fromStdString(ori.name()));
			ui.originX->setValue(ori.origin().getCoordinates(TCoordSysFactory::k3DCartesian).getX());
			ui.originY->setValue(ori.origin().getCoordinates(TCoordSysFactory::k3DCartesian).getY());
			ui.originZ->setValue(ori.origin().getCoordinates(TCoordSysFactory::k3DCartesian).getZ());
			ui.originBearing->setValue(ori.gisement().getGonsValue());
			ui.originSlope->setValue(ori.slope());
			fName = QString::fromStdString(ori.name());
		}
		catch (const std::exception &e)
		{
			logCritical() << e.what();
		}
	}
	else
	{
		QRegExp rx("([^\\x00-\\x7F])|([#%*\\s])"); //remove non ascii caracters, whitespaces and # and % reserved for comments and * reserved for keywords
		QString originNameClean = originName;
		originNameClean = originNameClean.remove(rx);
		ui.originName->setText(originNameClean);
		fName = originNameClean;
	}
	ui.originName->blockSignals(false);
	emit SIG_OriginChanged();
}

void OriginEditor::sXChanged(const double &originX)
{
	TPositionVector ori = fOrigin.getCoordinates(TCoordSysFactory::ECoordSys::k3DCartesian);
	fOrigin.setCoordinates(TPositionVector(originX, ori.getY(), ori.getZ(), ori.getCoordSys()));

	emit SIG_OriginChanged();
}

void OriginEditor::sYChanged(const double &originY)
{
	TPositionVector ori = fOrigin.getCoordinates(TCoordSysFactory::ECoordSys::k3DCartesian);
	fOrigin.setCoordinates(TPositionVector(ori.getX(), originY, ori.getZ(), ori.getCoordSys()));

	emit SIG_OriginChanged();
}

void OriginEditor::sZChanged(const double &originZ)
{
	TPositionVector ori = fOrigin.getCoordinates(TCoordSysFactory::ECoordSys::k3DCartesian);
	fOrigin.setCoordinates(TPositionVector(ori.getX(), ori.getY(), originZ, ori.getCoordSys()));

	emit SIG_OriginChanged();
}

void OriginEditor::sBearingChanged(const double &originBearing)
{
	fGisement = TAngle(originBearing, TAngle::EUnits::kGons);
	
	emit SIG_OriginChanged();
}

void OriginEditor::sSlopeChanged(const double &originSlope)
{
	fSlope = TAngle(originSlope, TAngle::EUnits::kRadians);

	emit SIG_OriginChanged();
}

bool OriginEditor::loadOrigin(std::shared_ptr<TDataParameters> fData)
{
	if (fData && fData->getLocalSystemOrigin())
	{
		blockSignals(true);
		fName = QString::fromStdString(fData->getLocalSystemOrigin()->name());
		fOrigin = fData->getLocalSystemOrigin()->origin();
		fGisement = fData->getLocalSystemOrigin()->gisement();
		fSlope = fData->getLocalSystemOrigin()->slope();

		ui.originName->setText(fName);
		ui.originBearing->setValue(fGisement.getGonsValue());
		ui.originSlope->setValue(fSlope.getRadiansValue());
		ui.originX->setValue(fOrigin.getCoordinates(TCoordSysFactory::ECoordSys::k3DCartesian).getX().getMetresValue());
		ui.originY->setValue(fOrigin.getCoordinates(TCoordSysFactory::ECoordSys::k3DCartesian).getY().getMetresValue());
		ui.originZ->setValue(fOrigin.getCoordinates(TCoordSysFactory::ECoordSys::k3DCartesian).getZ().getMetresValue());
		
		blockSignals(false);
		return true;
	}
	fName = "origin";
	fOrigin = TSpatialPosition(TRefFrameInfo::getReferenceFrame(TRefSystemFactory::kCCS), 0.0, 0.0, 0.0, TCoordSysFactory::k3DCartesian);
	fGisement = TAngle(0.0);
	fSlope = TAngle(0.0);

	ui.originName->clear();
	ui.originX->minimum();
	ui.originY->minimum();
	ui.originZ->minimum();
	ui.originBearing->minimum();
	ui.originSlope->minimum();
	return false;
}

bool OriginEditor::isComplete(TRefSystemFactory::ERefFrame ref)
{
	if (ref == TRefSystemFactory::ERefFrame::kMLA2000H0 || ref == TRefSystemFactory::ERefFrame::kMLA2000Machine || ref == TRefSystemFactory::ERefFrame::kMLA2000Topo
		|| ref == TRefSystemFactory::ERefFrame::kMLA1985H0 || ref == TRefSystemFactory::ERefFrame::kMLA1985Machine || ref == TRefSystemFactory::ERefFrame::kMLASphere
		|| ref == TRefSystemFactory::ERefFrame::kMLGGRS80 || ref == TRefSystemFactory::ERefFrame::kMLGSphere

	)
	{
		if (ui.originName->text() != "" && ui.originX->text() != ui.originX->specialValueText() && ui.originY->text() != ui.originY->specialValueText()
			&& ui.originZ->text() != ui.originZ->specialValueText() && ui.originSlope->text() != ui.originSlope->specialValueText()
			&& ui.originBearing->text() != ui.originBearing->specialValueText())
			return true;
		else
			return false;
	}
	else if (ref == TRefSystemFactory::ERefFrame::kLA1985H0 || ref == TRefSystemFactory::ERefFrame::kLA1985Machine || ref == TRefSystemFactory::ERefFrame::kLA2000H0
		|| ref == TRefSystemFactory::ERefFrame::kLA2000Machine || ref == TRefSystemFactory::ERefFrame::kLA2000Topo || ref == TRefSystemFactory::ERefFrame::kLASphere
		|| ref == TRefSystemFactory::ERefFrame::kLGGRS80 || ref == TRefSystemFactory::ERefFrame::kLGSphere) // no bearing and slope
	{
		if (ui.originName->text() != "" && ui.originX->text() != ui.originX->specialValueText() && ui.originY->text() != ui.originY->specialValueText()
			&& ui.originZ->text() != ui.originZ->specialValueText())
			return true;
		else
			return false;
	}
	else
		return false;
}

void OriginEditor::setDataParameter(std::shared_ptr<TDataParameters> params)
{
	if (isComplete(params->getRefFrameEnumerator()))
	{
		fOrigin = params->getLocalSystemOrigin()->origin();
		fGisement = params->getLocalSystemOrigin()->gisement();
		fSlope = params->getLocalSystemOrigin()->slope();
		fName = QString::fromStdString(params->getLocalSystemOrigin()->name());

		emit SIG_OriginChanged();
	}
}
