/*!
	OriginEditor.h

	Class defining the origin point from QLineEdit.
*/

#ifndef TORIGIN_EDITOR_5E249539792B47da9B96C5BB3CE512C9
#define TORIGIN_EDIOTR_5E249539792B47da9B96C5BB3CE512C9

#include <ui_OriginEditor.h>
#include <TDataParameters.h>
#include <TLocalSystemOrigin.h>
#include <memory>

class OriginEditor : public QWidget
{
	Q_OBJECT

public:

	OriginEditor(QWidget * parent = 0);
	void setBearing(bool enable);
	Ui::OriginEditor ui;

	// Allows to set the origin in the UI when we load a file
	bool loadOrigin(std::shared_ptr<TDataParameters> fData);

	//check if origin is complety fill in
	bool isComplete(TRefSystemFactory::ERefFrame);
	// update dataParameter from the fileSettingsPanel
	void setDataParameter(std::shared_ptr<TDataParameters> params);

	//return the local system origin
	//std::shared_ptr<TLocalSystemOrigin> getOrigin(){ return fDataParams->getLocalSystemOrigin(); };
	TLocalSystemOrigin getOrigin() const { return TLocalSystemOrigin(fOrigin, fGisement, fSlope, fName.toStdString()); };

signals:

	// signal emitted when the origin has been changed
	void SIG_OriginChanged();

private:

	bool rotationEnabled;
	TSpatialPosition fOrigin;
	TAngle fGisement;
	TAngle fSlope;
	QString fName;

private slots:

	void sNameChanged(const QString&);
	void sXChanged(const double&);
	void sYChanged(const double&);
	void sZChanged(const double&);
	void sBearingChanged(const double&);
	void sSlopeChanged(const double&);
	
};

#endif
