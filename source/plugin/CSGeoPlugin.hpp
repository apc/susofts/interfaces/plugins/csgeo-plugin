/*
© Copyright CERN 2022. Al2 rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CSGEOPLUGIN_HPP
#define CSGEOPLUGIN_HPP

#include <memory>

#include <interface/SPluginInterface.hpp>

/**
 * Plugin for CSGeo.
 *
 * This plugin is meant to be used with SurveyPad.
 *
 * This class implements the plugin interface for SurveyPad.
 *
 * This plugin does not use external interfaces or executables to run the files, all the calculations are
 * computed with the source code in CSGeoCore library and SurveyLib library.
 */
class CSGeoPlugin : public QObject, public SPluginInterface
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID SPluginInterfaceIID)
	Q_INTERFACES(SPluginInterface)

public:
	CSGeoPlugin(QObject *parent = nullptr);
	virtual ~CSGeoPlugin() override;

	virtual const QString &name() const noexcept override { return CSGeoPlugin::_name(); }
	virtual QIcon icon() const override;

	virtual void init() override;
	virtual void terminate() override;

	virtual bool hasConfigInterface() const noexcept override { return true; }
	virtual SConfigWidget *configInterface(QWidget *parent = nullptr) override;
	virtual bool hasGraphicalInterface() const noexcept override { return true; }
	virtual SGraphicalWidget *graphicalInterface(QWidget *parent = nullptr) override;
	virtual bool hasLaunchInterface() const noexcept override { return true; }
	virtual SLaunchObject *launchInterface(QObject *parent = nullptr) override;

	virtual QString getExtensions() const noexcept override { return tr("CSGeo (*.csgeo)"); }
	virtual bool isMonoInstance() const noexcept override { return false; }

public:
	static const QString &_name();
	static const QString &_baseMimetype();

private:
	class _CSGeoPlugin_pimpl;
	std::unique_ptr<_CSGeoPlugin_pimpl> _pimpl;
};

#endif // CSGEOPLUGIN_HPP
