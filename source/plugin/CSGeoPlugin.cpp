#include "CSGeoPlugin.hpp"

#include <Logger.hpp>

#include "trinity/CSGeoConfig.hpp"
#include "trinity/CSGeoGraphicalWidget.hpp"
#include "trinity/CSGeoLaunchObject.hpp"
#include "Version.hpp"

class CSGeoPlugin::_CSGeoPlugin_pimpl
{
};

CSGeoPlugin::CSGeoPlugin(QObject *parent) : QObject(parent), SPluginInterface(), _pimpl(std::make_unique<_CSGeoPlugin_pimpl>())
{
}

CSGeoPlugin::~CSGeoPlugin() = default;

QIcon CSGeoPlugin::icon() const
{
	return QIcon(":/icons/csgeo");
}

void CSGeoPlugin::init()
{
	logDebug() << "Plugin CSGeo loaded";
}

void CSGeoPlugin::terminate()
{
	logDebug() << "Plugin CSGeo unloaded";
}

SConfigWidget *CSGeoPlugin::configInterface(QWidget *parent)
{
	logDebug() << "loading CSGeoPlugin::configInterface (CSGeoConfig)";
	return new CSGeoConfig(this, parent);
}

SGraphicalWidget *CSGeoPlugin::graphicalInterface(QWidget *parent)
{
	logDebug() << "loading CSGeolugin::graphicalInterface (CSGeoGraphicalWidget)";
	return new CSGeoGraphicalWidget(this, parent);
}

SLaunchObject *CSGeoPlugin::launchInterface(QObject *parent)
{
	logDebug() << "loading CSGeoPlugin::launchInterface (CSGeoLaunchObject)";
	return new CSGeoLaunchObject(this, parent);
}

const QString &CSGeoPlugin::_name()
{
	static const QString _sname = QString::fromStdString(getPluginName());
	return _sname;
}

const QString &CSGeoPlugin::_baseMimetype()
{
	static const QString _sbaseMimetype = "application/vnd.cern.susoft.surveypad.plugin.csgeo";
	return _sbaseMimetype;
}
