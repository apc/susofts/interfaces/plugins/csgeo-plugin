/*
© Copyright CERN 2000-2022. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CSGEOCONFIG_HPP
#define CSGEOCONFIG_HPP

#include <memory>

#include <interface/SConfigInterface.hpp>

namespace Ui
{
class CSGeoConfig;
}

struct CSGeoConfigObject : public SConfigObject
{
	virtual ~CSGeoConfigObject() override = default;

	bool operator==(const SConfigObject &o) const noexcept override;

	virtual void write() const override;
	virtual void read() override;

	/** If true,input and output files should be automatically overwritten */
	bool disableCheckOverwrite = false;
};

class CSGeoConfig : public SConfigWidget
{
	Q_OBJECT

public:
	CSGeoConfig(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~CSGeoConfig();

	// SConfigInterface
	virtual SConfigObject *config() const override;
	virtual void setConfig(const SConfigObject *conf) override;
	virtual void reset() override;
	virtual void restoreDefaults() override;

private:
	std::unique_ptr<Ui::CSGeoConfig> ui;
};

#endif // CSGEOCONFIG_HPP
