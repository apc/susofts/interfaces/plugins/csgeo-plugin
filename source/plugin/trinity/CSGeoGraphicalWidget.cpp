#include "CSGeoGraphicalWidget.hpp"

#include <fstream>
#include <memory>

#include <QFileInfo>
#include <QGridLayout>
#include <QPushButton>
#include <QScrollArea>
#include <QSplitter>
#include <QMessageBox>

#include <Behavior.h>
#include <CSGeoCore/headers/ProjectCore/DataSet.h>
#include <CSGeoCore/headers/ProjectCore/Project.h>
#include <CSGeoCore/headers/io/input_point/TPointReaderFactory.h>
#include <Logger.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <StringManager.h>
#include <editors/text/TextEditor.hpp>
#include <editors/text/TextUtils.hpp>
#include <interface/SPluginInterface.hpp>
#include <tabs/inputs/CSGeoInputWidget.hpp>
#include <tabs/outputs/CSGeoOutputTabsWidget.hpp>
#include <utils/SettingsManager.hpp>
#include <trinity/CSGeoConfig.hpp>

#include "CSGeoPlugin.hpp"

class CSGeoGraphicalWidget::_CSGeoGraphicalWidget_pimpl
{
public:
	/** Project */
	std::unique_ptr<Project> project = nullptr;
	/** input text editor */
	CSGeoInputWidget *input = nullptr;
	/** output tabs */
	CSGeoOutputTabsWidget *output = nullptr;
};

CSGeoGraphicalWidget::CSGeoGraphicalWidget(SPluginInterface *owner, QWidget *parent) :
	SGraphicalScriptWidget(owner, parent), _pimpl(std::make_unique<_CSGeoGraphicalWidget_pimpl>())
{
	setupEditors();
	setupScripts();

	// Connections
	connect(_pimpl->output, &CSGeoOutputTabsWidget::tabAddRequested, this, &CSGeoGraphicalWidget::addNewOutput);
	connect(_pimpl->output, &CSGeoOutputTabsWidget::tabCloseRequested, this, &CSGeoGraphicalWidget::removeOutput);
	connect(_pimpl->output, &SGraphicalWidget::hasChanged, this, &SGraphicalWidget::hasChanged);

	connect(_pimpl->input, &SGraphicalWidget::hasChanged, this, &SGraphicalWidget::hasChanged);
	connect(_pimpl->input, &SGraphicalWidget::copyAvailable, this, &SGraphicalWidget::copyAvailable);
	connect(_pimpl->input, &SGraphicalWidget::cutAvailable, this, &SGraphicalWidget::cutAvailable);
	connect(_pimpl->input, &SGraphicalWidget::undoAvailable, this, &SGraphicalWidget::undoAvailable);
	connect(_pimpl->input, &SGraphicalWidget::redoAvailable, this, &SGraphicalWidget::redoAvailable);
	connect(_pimpl->input, &SGraphicalWidget::statusWidgetChanged, this, &SGraphicalWidget::statusWidgetChanged);
}

CSGeoGraphicalWidget::~CSGeoGraphicalWidget() = default;

QString CSGeoGraphicalWidget::getPath() const
{
	return QString::fromStdWString(_pimpl->project->getAbsolutePath());
}

ShareablePointsList CSGeoGraphicalWidget::getContent() const
{
	return ShareablePointsList();
}

QWidget *CSGeoGraphicalWidget::statusWidget()
{
	return _pimpl->input->statusWidget();
}

QWidget *CSGeoGraphicalWidget::helpWidget()
{
	return _pimpl->input->helpWidget();
}

QToolBar *CSGeoGraphicalWidget::toolbar()
{
	return _pimpl->input->toolbar();
}

bool CSGeoGraphicalWidget::isModified() const noexcept
{
	return _pimpl->input->isModified() || _pimpl->output->isModified();
}

void CSGeoGraphicalWidget::isModified(bool modified) noexcept
{
	_pimpl->input->isModified(modified);
	_pimpl->output->isModified(modified);
}

void CSGeoGraphicalWidget::runFinished(bool status)
{
	if (!_pimpl->input->open(QString::fromWCharArray(_pimpl->project->getInputFileInfo().first.c_str())))
	{
		logCritical() << "Can't open input file!";
	}
	if (!_pimpl->output->openOutputs())
	{
		logCritical() << "Can't open one or more output files!";
	}
}

bool CSGeoGraphicalWidget::_save(const QString &path)
{
	// Before save CSGeo needs to:
	// 	   - read the input
	// 	   - run and generate the output (if there are any)
	// 	   - save all the files

	// read in the points
	std::unique_ptr<TVPointReader> pointReader;
	try
	{
		pointReader = TPointReaderFactory::getPointReader(*_pimpl->input->getDataParameters());
	}
	catch (const std::exception &e)
	{
		logCritical() << e.what();
		return false;
	}
	std::vector<TSpatialPoint> points;
	for (int i = 0; i < _pimpl->input->editor()->lines(); i++)
	{
		std::string line = _pimpl->input->editor()->text(i).trimmed().toStdString();
		if (line[0] == '#' || line.empty())
			continue;
		std::istringstream iss(line.c_str());
		std::istream &input(iss);
		TSpatialPoint point = pointReader->readPoint(input);
		if (pointReader->hasError())
		{
			logWarning() << "Failed to parse line #" << i << ". Error: can't read '" << line << "'\n";
			pointReader->clearError();
		}
		points.push_back(point);
	}
	// If no points - cancel
	_pimpl->project->getInputDataSet()->setPoints(points);
	if (points.empty())
	{
		logCritical() << "Cannot save: there are no points defined!";
		return false;
	}

	// Updates all the project paths (including input and output)
	_pimpl->project->setAbsolutePath(path.toStdWString());


	// There is try-catch as well as for-loop for @Behavior since some errors
	// are logged by CSGeoCore but some are not handled properly

	// Run project first
	Behavior beh_run;
	try
	{
		beh_run = _pimpl->project->run();
	}
	catch (const std::exception &e)
	{
		logCritical() << e.what();
		return false;
	}
	for (Behavior behavior_error : beh_run.split())
		logWarning() << toStr(Behavior::message(behavior_error.code()) + L": " + behavior_error.additionalInfo(behavior_error.code()));
	if (!beh_run.getType())
		return false;

	// Save project, input and outputs
	Behavior beh_save;
	try
	{
		// If automatic overwrite unchecked and input or output files exist
		auto config = SettingsManager::settings().settings<CSGeoConfigObject>(CSGeoPlugin::_name());
		if (!config.disableCheckOverwrite)
		{
			QFileInfo input = QString::fromStdWString(_pimpl->project->getInputDataSet()->getAbsolutePath());
			if (input.exists())
			{
				auto answer = QMessageBox::question(this, tr("Overwriting input file"),
					tr("Warning: the input file \"%1\" will overwrite an existing file in the directory. Do you want to continue?").arg(input.fileName()),	QMessageBox::Yes | QMessageBox::No);
				if (answer == QMessageBox::No)
					return false;
			}

			std::vector<std::shared_ptr<DataSet>> const & outputDatasets = _pimpl->project->getOutputDataSets();
			for (std::shared_ptr<DataSet> dataset : outputDatasets)
			{
				QFileInfo output = QString::fromStdWString(dataset.get()->getAbsolutePath());
				if (output.exists())
				{ 
					auto answer = QMessageBox::question(this, tr("Overwriting output file"),
						tr("Warning: the output file \"%1\" will overwrite an existing file in the directory. Do you want to continue? ").arg(output.fileName()), QMessageBox::Yes | QMessageBox::No);
					if (answer == QMessageBox::No)
						return false;
				}
			}
		}
		beh_save = _pimpl->project->save();
	}
	catch (const std::exception &e)
	{
		logCritical() << e.what();
		return false;
	}
	for (Behavior behavior_save : beh_save.split())
		logWarning() << toStr(Behavior::message(behavior_save.code()) + L": " + behavior_save.additionalInfo(behavior_save.code()));
	if (!beh_save)
		return false;

	// Force reopen outputs
	if (!_pimpl->output->openOutputs())
	{
		logCritical() << "Can't open one or more output files!";
		return false;
	}

	updateWindowTitle();
	return true;
}

bool CSGeoGraphicalWidget::_open(const QString &path)
{
	// Project
	_pimpl->project.reset(new Project(path.toStdWString()));
	_pimpl->input->setDataSet(_pimpl->project->getInputDataSet());
	Behavior beh_read = _pimpl->project->readStatus();
	for (Behavior behavior_open : beh_read.split())
	{
		logWarning() << toStr(Behavior::message(behavior_open.code()) + L": " + behavior_open.additionalInfo(behavior_open.code()));
	}
	if (!beh_read)
	{
		logCritical() << "Could not open the project or its input/output files. The files cannot contain any `nan` values";
		return false;
	}

	// Input
	bool b = _pimpl->input->open(QString::fromWCharArray(_pimpl->project->getInputFileInfo().first.c_str()));
	if (!b)
	{
		logCritical() << "Can't open input file" << toStr(_pimpl->project->getInputFileInfo().second);
		return b;
	}

	// Outputs
	for (std::shared_ptr<DataSet> dataset : _pimpl->project->getOutputDataSets())
		_pimpl->output->addTab(dataset);
	b &= _pimpl->output->openOutputs();
	if (!b)
	{
		logCritical() << "Can't open one or more output files!";
		return b;
	}

	updateWindowTitle();
	return b;
}

void CSGeoGraphicalWidget::setupEditors()
{
	// Project
	_pimpl->project = std::make_unique<Project>();

	// Layout
	QWidget *widget = new QWidget(this);

	// Scroll
	QVBoxLayout *scroll_layout = new QVBoxLayout(widget);
	QScrollArea *scrollArea = new QScrollArea(widget);
	scrollArea->setWidgetResizable(true);
	scrollArea->setFrameShape(QFrame::NoFrame);
	scroll_layout->addWidget(scrollArea);
	widget->setLayout(scroll_layout);

	// Contents
	QVBoxLayout *v_layout = new QVBoxLayout(scrollArea);
	QSplitter *splitter = new QSplitter(this);
	v_layout->addWidget(splitter);
	_pimpl->input = new CSGeoInputWidget(owner(), _pimpl->project->getInputDataSet(), this);
	splitter->addWidget(_pimpl->input);
	_pimpl->output = new CSGeoOutputTabsWidget(owner(), this);
	splitter->addWidget(_pimpl->output);
	_pimpl->output->newEmpty();
	splitter->setSizes({this->size().width(), this->size().width()}); // 1:1 ratio

	// Set widget
	setWidget(widget);
}

void CSGeoGraphicalWidget::updateWindowTitle()
{
	QString title = QFileInfo(QString::fromStdWString(_pimpl->project->getAbsolutePath())).baseName();
	if (title.isEmpty())
		title = tr("New_Project");
	setWindowTitle(title);
}

void CSGeoGraphicalWidget::addNewOutput()
{
	_pimpl->project->addEmptyOuputDataSet();
	_pimpl->output->addTab(_pimpl->project->getOutputDataSets().back());
}

void CSGeoGraphicalWidget::removeOutput(int index)
{
	_pimpl->project->removeOutputDataSet(index);
}

void CSGeoGraphicalWidget::_newEmpty()
{
	_pimpl->project->addEmptyOuputDataSet();
	_pimpl->input->newEmpty();
	_pimpl->output->addTab(_pimpl->project->getOutputDataSets().back());
	updateWindowTitle();
}
