/*
© Copyright CERN 2022. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CSGEOGRAPHICALWIDGET_HPP
#define CSGEOGRAPHICALWIDGET_HPP

#include <memory>

#include <editors/script/EditorScriptWidgets.hpp>

/**
 * Graphical interface for CSGeo Plugin.
 *
 * This interface implements the graphical interface for SurveyPad plugins.
 *
 * It is composed of a single widget, that contains two subwidgets:
 * - input - @CSGeoInputWidget inherits from @TextEditorWidget and adds @TFileSettingsPanel on top. This is the only `active`
 *	widget, i.e. all the edit functionalities (copy, undo etc.) are tied to @CSGeoGraphicalWidget to propagate the signal.
 *	Moreover, all the help widgets (status, toolbar etc.) are provided to @CSGeoGraphicalWidget by @CSGeoInputWidget.
 * - output - @CSGeoOutputTabsWidget inherits from @TabEditorWidget and contains zero or more @CSGeoOutputWidget that inherit from
 *	@SGraphicalWidget. This class is similar to @CSGeoInputWidget since it also has @TFileSettingsPanel on top, but this class
 *	is only used to open and view files, and therefore, the editing functionality is not useful. Only interactive element of this
 *	class is @TFileSettingsPanel that can be changed.
 *
 * Moreover, for save, open, run functionalities the code from old CSGeo was reused not to change the results of computations.
 * All this functionality is located in @Project class that actually manages the files in the plugin. Only minor changes were
 * done to improve functionality or compatibility on Linux.
 */
class CSGeoGraphicalWidget : public SGraphicalScriptWidget
{
	Q_OBJECT

public:
	CSGeoGraphicalWidget(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~CSGeoGraphicalWidget() override;

	// SGraphicalWidget
	virtual QString getPath() const override;
	virtual ShareablePointsList getContent() const override;
	virtual QWidget *statusWidget() override;
	virtual QWidget *helpWidget() override;
	virtual QToolBar *toolbar() override;
	virtual bool isModified() const noexcept override;

public slots:
	// SGraphicalWidget
	virtual void isModified(bool modified) noexcept;
	virtual void runFinished(bool status) override;

protected:
	// Inherited via SGraphicalWidget
	virtual bool _save(const QString &path) override;
	virtual bool _open(const QString &path) override;
	virtual void _newEmpty() override;

private:
	/** Setup of CSGeo editors */
	void setupEditors();
	/** Update the window title with the name of the project file */
	void updateWindowTitle();
	/** New tab */
	void addNewOutput();
	/** Remove tab */
	void removeOutput(int index);

private:
	/** pimpl */
	class _CSGeoGraphicalWidget_pimpl;
	std::unique_ptr<_CSGeoGraphicalWidget_pimpl> _pimpl;
};

#endif // CSGEOGRAPHICALWIDGET_HPP
