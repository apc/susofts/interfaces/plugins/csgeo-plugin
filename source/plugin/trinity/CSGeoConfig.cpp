#include "CSGeoConfig.hpp"
#include "ui_CSGeoConfig.h"

#include <QSettings>

#include "CSGeoPlugin.hpp"
#include "Version.hpp"

bool CSGeoConfigObject::operator==(const SConfigObject &o) const noexcept
{
	if (!SConfigObject::operator==(o))
		return false;
	const CSGeoConfigObject &oc = static_cast<const CSGeoConfigObject &>(o);
	return disableCheckOverwrite == oc.disableCheckOverwrite;
}

void CSGeoConfigObject::write() const
{
	QSettings settings;
	settings.beginGroup(CSGeoPlugin::_name());

	settings.setValue("checkOverwrite", disableCheckOverwrite);

	settings.endGroup();
}

void CSGeoConfigObject::read()
{
	QSettings settings;
	settings.beginGroup(CSGeoPlugin::_name());

	disableCheckOverwrite = settings.value("checkOverwrite", false).toBool();

	settings.endGroup();
}

CSGeoConfig::CSGeoConfig(SPluginInterface *owner, QWidget *parent) : SConfigWidget(owner, parent), ui(std::make_unique<Ui::CSGeoConfig>())
{
	ui->setupUi(this);
	ui->lblVersion->setText(ui->lblVersion->text().arg(QString::fromStdString(getVersion())));
}

CSGeoConfig::~CSGeoConfig() = default;

SConfigObject *CSGeoConfig::config() const
{
	auto *tmp = new CSGeoConfigObject();
	tmp->disableCheckOverwrite = ui->checkOverwrite->isChecked();
	return tmp;
}

void CSGeoConfig::setConfig(const SConfigObject *conf)
{
	const auto *config = dynamic_cast<const CSGeoConfigObject *>(conf);
	if (config)
	{
		ui->checkOverwrite->setChecked(config->disableCheckOverwrite);
	}
}

void CSGeoConfig::reset()
{
	CSGeoConfigObject config;
	config.read();
	setConfig(&config);
}

void CSGeoConfig::restoreDefaults()
{
	CSGeoConfigObject config;
	setConfig(&config);
}
