/*
© Copyright CERN 2022. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CSGEOLAUNCHOBJECT_HPP
#define CSGEOLAUNCHOBJECT_HPP

#include <interface/ProcessLauncherObject.hpp>

/**
 * Launcher for CSGeo plugin.
 *
 * It does not exactly launch any process and in this regard it is a dummy. To preserve the same functionality
 * as other plugins have it emits @SLaunchObject::finished(true) on completion but the calculations are in
 * fact done by creating a new @Project file from path, running it and closing, as it was done in the old
 * command line interface. Therefore, exactly the same Project should be created as the one in @CSGeoGrapgicalWidget
 * inside of @launch method.
 */
class CSGeoLaunchObject : public ProcessLauncherObject
{
	Q_OBJECT

public:
	CSGeoLaunchObject(SPluginInterface *owner, QObject *parent = nullptr) : ProcessLauncherObject(owner, parent) {}
	virtual ~CSGeoLaunchObject() override = default;

	// SLaunchObject
	virtual QString exepath() const override { return "INTERNAL_COMPUTATIONS"; };

public slots:
	// ProcessLauncherObject
	virtual void launch(const QString &file) override;
};

#endif // CSGEOLAUNCHOBJECT_HPP
