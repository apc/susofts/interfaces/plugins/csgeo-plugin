#include "CSGeoLaunchObject.hpp"

#include <QTimer>

#include <CSGeoCore/headers/ProjectCore/Project.h>
#include <Logger.hpp>

#include "CSGeoPlugin.hpp"
#include "StringManager.h"

void CSGeoLaunchObject::launch(const QString &file)
{
	Project project(file.toStdWString());

	Behavior beh_run = project.run();
	for (Behavior behavior_error : beh_run.split())
	{
		logCritical() << toStr(Behavior::message(behavior_error.code()));
		emit SLaunchObject::finished(false);
		QTimer::singleShot(200, this, [this]() { emit SLaunchObject::finished(false); }); // delay the signal so that it can be caught
	}

	Behavior beh_save = project.save();
	for (Behavior behavior_save : beh_save.split())
	{
		logCritical() << toStr(Behavior::message(behavior_save.code()));
		QTimer::singleShot(200, this, [this]() { emit SLaunchObject::finished(false); }); // delay the signal so that it can be caught
	}

	QTimer::singleShot(200, this, [this]() { emit SLaunchObject::finished(true); }); // delay the signal so that it can be caught
}
